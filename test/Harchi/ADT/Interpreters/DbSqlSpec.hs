
{-# LANGUAGE FlexibleContexts #-}

module Harchi.ADT.Interpreters.DbSqlSpec where

import Control.Monad.Writer (MonadIO, WriterT, MonadWriter, tell, runWriterT)
import Test.Hspec

import Harchi.ADT.Domain.Person (Person(..))
import Harchi.ADT.Effects.Db (Db(..))
import Harchi.ADT.Effects.Logger (Logger(..))
import Harchi.ADT.Interpreters.DbSql (mkDbSql)

sqlFile :: FilePath
sqlFile = "persons.db"

person1, person2 :: Person
person1 = Person 1 "Haskell Curry" "USA"
person2 = Person 2 "Alan Turing" "GB"

persons :: [Person]
persons = [person1, person2]

runApp :: 
  (MonadWriter [String] m1, MonadIO m1) =>
  (Db m1 -> WriterT [String] m2 a) -> m2 (a, [String])
runApp app = runWriterT (app db)
  where
    logFunc str = tell [str]
    logger = Logger logFunc
    db = mkDbSql sqlFile logger

app1 :: (MonadIO m, MonadWriter [String] m) => Db m -> m [Person]
app1 = getPersons 

app2 :: (MonadIO m, MonadWriter [String] m) => Db m -> m [Person]
app2 db = getPersonFromId db 1

app3 :: (MonadIO m, MonadWriter [String] m) => Db m -> m [Person]
app3 db = getPersonFromId db 42

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Interpreters DbSql" $ do

    it "getPersons" $ do
      res <- runApp app1 
      res `shouldBe` (persons, ["Interpreters DbSql getPersons"])

    it "getPersonFromId 1" $ do
      res <- runApp app2 
      res `shouldBe` ([person1], ["Interpreters DbSql getPersonFromId 1"])

    it "getPersonFromId 42" $ do
      res <- runApp app3 
      res `shouldBe` ([], ["Interpreters DbSql getPersonFromId 42"])

