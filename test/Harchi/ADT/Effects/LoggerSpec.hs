
{-# LANGUAGE FlexibleContexts #-}

{-# OPTIONS_GHC -Wno-orphans #-} 

module Harchi.ADT.Effects.LoggerSpec where

import Control.Monad.Writer (MonadWriter, Writer, runWriter, tell)
import Test.Hspec

import Harchi.ADT.Effects.Logger (Logger(..))

runApp :: 
  MonadWriter [String] m =>
  (Logger m -> Writer [String] a) -> (a, [String])
runApp app = runWriter (app logger)
  where
    logFunc str = tell [str]
    logger = Logger logFunc

app1 :: (MonadWriter [String] m) => Logger m -> m ()
app1 logger = do
  logMsg logger "foo"
  logMsg logger "bar"

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Effects Logger" $ do
    it "foo bar" $ runApp app1 `shouldBe` ((), ["foo", "bar"])

