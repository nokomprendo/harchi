
{-# LANGUAGE FlexibleContexts #-}

module Harchi.ADT.Effects.DbSpec where

import Control.Monad.Reader (MonadReader, Reader, runReader, ask, asks)
import Test.Hspec

import Harchi.ADT.Domain.Person (Person(..))
import Harchi.ADT.Effects.Db (Db(..))

person1, person2 :: Person
person1 = Person 1 "Ada Lovelace" "GB"
person2 = Person 2 "Magaret Hamilton" "USA"

persons :: [Person]
persons = [person1, person2]

runApp :: 
  MonadReader [Person] m =>
  (Db m -> Reader [Person] a) -> a
runApp app = runReader (app db) persons
  where
    _getPersons = ask
    _getPersonFromId i = asks $ filter ((==i) . personId)
    db = Db _getPersons _getPersonFromId

app1 :: (MonadReader [Person] m) => Db m -> m [Person]
app1 = getPersons 

app2 :: (MonadReader [Person] m) => Db m -> m [Person]
app2 db = getPersonFromId db 1

app3 :: (MonadReader [Person] m) => Db m -> m [Person]
app3 db = getPersonFromId db 42

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Effects Db" $ do
    it "getPersons" $ runApp app1 `shouldBe` persons
    it "getPersonFromId 1" $ runApp app2 `shouldBe` [person1]
    it "getPersonFromId 42" $ runApp app3 `shouldBe` []

