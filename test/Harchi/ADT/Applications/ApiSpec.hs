
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Harchi.ADT.Applications.ApiSpec where

import Network.Wai (Application)
import Servant.Server.Generic (genericServe)
import Test.Hspec
import Test.Hspec.Wai (with, get, shouldRespondWith)
import Test.Hspec.Wai.JSON (json)

import Harchi.ADT.Domain.Person (Person(..))
import Harchi.ADT.Effects.Logger (Logger(..))
import Harchi.ADT.Interpreters.DbSql (mkDbSql)
import Harchi.ADT.Applications.Api (serverRecord)

sqlFile :: FilePath
sqlFile = "persons.db"

persons :: [Person]
persons = 
  [ Person 1 "Haskell Curry" "USA"
  , Person 2 "Alan Turing" "GB"
  ]

serverApp :: IO Application 
serverApp = 
  return $ genericServe (serverRecord logger db)
    where
      logger = Logger $ \_ -> pure () 
      db = mkDbSql sqlFile logger

spec :: Spec
spec = do

  with serverApp $

    describe "Applications Api" $ do
      
      it "person 2" $ do
        get "/person/2" `shouldRespondWith` 
          [json|
            [ {personId: 2, personName: "Alan Turing", personCountry: "GB"}
            ]
          |]

      it "home" $ do
        get "/" `shouldRespondWith` "\"hello\""

      it "person 42" $ do
        get "/person/42" `shouldRespondWith` "[]"

      it "person 1" $ do
        get "/person/1" `shouldRespondWith` 
          "[{\"personId\":1,\"personName\":\"Haskell Curry\",\"personCountry\":\"USA\"}]"


      it "persons" $ do
        get "/persons" `shouldRespondWith` 
          [json|
            [ {personId: 1, personName: "Haskell Curry", personCountry: "USA"}
            , {personId: 2, personName: "Alan Turing", personCountry: "GB"}
            ]
          |]

main :: IO ()
main = hspec spec

