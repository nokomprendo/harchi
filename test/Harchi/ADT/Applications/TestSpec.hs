
{-# LANGUAGE FlexibleContexts #-}

module Harchi.ADT.Applications.TestSpec where

import Control.Monad.Writer (WriterT, tell, runWriterT)
import Test.Hspec

import Harchi.ADT.Domain.Person (Person(..))
import Harchi.ADT.Effects.Logger (Logger(..))
import Harchi.ADT.Interpreters.DbSql (mkDbSql)
import Harchi.ADT.Applications.Test (testApp, TestApp)

sqlFile :: FilePath
sqlFile = "persons.db"

person1, person2 :: Person
person1 = Person 1 "Haskell Curry" "USA"
person2 = Person 2 "Alan Turing" "GB"

persons :: [Person]
persons = [person1, person2]

type WriterIO = WriterT [String] IO

runTestApp :: TestApp WriterIO [Person] -> IO ([Person], [String])
runTestApp app = runWriterT (app logger db)
  where
    logFunc str = tell [str]
    logger = Logger logFunc
    db = mkDbSql sqlFile logger

spec :: Spec
spec = do

  describe "Applications Test" $ do

    it "logs + db" $ do
      (ps, logs) <- runTestApp testApp 
      ps `shouldBe` (person2 : persons)
      logs `shouldBe` 
        [ "Applications Test testApp getPerson 2"
        , "Interpreters DbSql getPersonFromId 2"
        , "Applications Test testApp getPersons"
        , "Interpreters DbSql getPersons"]

main :: IO ()
main = hspec spec

