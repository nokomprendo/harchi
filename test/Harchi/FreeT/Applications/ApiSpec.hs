
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Harchi.FreeT.Applications.ApiSpec where

import Control.Monad.Trans.Free (FreeT(..), FreeF(..))
import Network.Wai (Application)
import Servant (Handler(..))
import Servant.Server.Generic (genericServeT)
import Test.Hspec
import Test.Hspec.Wai (with, get, shouldRespondWith)
import Test.Hspec.Wai.JSON (json)

import Harchi.FreeT.Domain.Person (Person(..))
import Harchi.FreeT.Effects.Logger (LoggerF(..), LoggerT)
import Harchi.FreeT.Effects.Db (DbT)
import Harchi.FreeT.Interpreters.DbSql (runDbSql)
import Harchi.FreeT.Applications.Api (serverRecord)

sqlFile :: FilePath
sqlFile = "persons.db"

persons :: [Person]
persons = 
  [ Person 1 "Haskell Curry" "USA"
  , Person 2 "Alan Turing" "GB"
  ]

runLoggerNull :: Monad m => LoggerT m r -> m r
runLoggerNull app = do
  x0 <- runFreeT app
  case x0 of
    Pure r -> return r
    Free (LogMsg _str next) -> do
      runLoggerNull (next ())

serverApp :: IO Application 
serverApp = 
  return $ genericServeT runApp serverRecord
    where
      runApp :: DbT (LoggerT Handler) r -> Handler r
      runApp app = runLoggerNull $ runDbSql sqlFile app

spec :: Spec
spec = do

  with serverApp $

    describe "Applications Api" $ do

      it "person 2" $ do
        get "/person/2" `shouldRespondWith` 
          [json|
            [ {personId: 2, personName: "Alan Turing", personCountry: "GB"}
            ]
          |]

      it "home" $ do
        get "/" `shouldRespondWith` "\"hello\""

      it "person 42" $ do
        get "/person/42" `shouldRespondWith` "[]"

      it "person 1" $ do
        get "/person/1" `shouldRespondWith` 
          "[{\"personId\":1,\"personName\":\"Haskell Curry\",\"personCountry\":\"USA\"}]"

      it "persons" $ do
        get "/persons" `shouldRespondWith` 
          [json|
            [ {personId: 1, personName: "Haskell Curry", personCountry: "USA"}
            , {personId: 2, personName: "Alan Turing", personCountry: "GB"}
            ]
          |]

main :: IO ()
main = hspec spec

