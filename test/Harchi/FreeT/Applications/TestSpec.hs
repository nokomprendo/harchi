
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}

module Harchi.FreeT.Applications.TestSpec where

import Control.Monad (join)
import Control.Monad.Trans.Free -- (FreeT(..), FreeF(..))
import Control.Monad.Writer (MonadWriter, tell)
import Control.Monad.Trans.Writer (runWriterT, WriterT)
import Test.Hspec

import Harchi.FreeT.Domain.Person (Person(..))
import Harchi.FreeT.Effects.Db (DbT)
import Harchi.FreeT.Effects.Logger (LoggerF(..), LoggerT)
import Harchi.FreeT.Interpreters.DbSql (runDbSql)
import Harchi.FreeT.Applications.Test (testApp)

sqlFile :: FilePath
sqlFile = "persons.db"

person1, person2 :: Person
person1 = Person 1 "Haskell Curry" "USA"
person2 = Person 2 "Alan Turing" "GB"

persons :: [Person]
persons = [person1, person2]

-- runLoggerWriter :: MonadWriter [String] m => LoggerT m r -> m r
-- runLoggerWriter app = do
--   x0 <- runFreeT app
--   case x0 of
--     Pure r -> return r
--     Free (LogMsg str next) -> do
--       tell [str]
--       runLoggerWriter (next ())

runLoggerWriter :: MonadWriter [String] m => LoggerT m a -> m a
runLoggerWriter = iterT (join . interpretLoggerWriter)
  where
    interpretLoggerWriter = \case
      LogMsg str next -> do
        tell [str]
        pure $ next ()

runTestApp :: DbT (LoggerT (WriterT [String] IO)) [Person] -> IO ([Person], [String])
runTestApp app = runWriterT $ runLoggerWriter $ runDbSql sqlFile app

spec :: Spec
spec = do

  describe "Applications Test" $ do

    it "logs + db" $ do
      (ps, logs) <- runTestApp testApp 
      ps `shouldBe` (person2 : persons)
      logs `shouldBe` 
        [ "Applications Test testApp getPerson 2"
        , "Interpreters DbSql interpretDbSql GetPersonFromId 2"
        , "Applications Test testApp getPersons"
        , "Interpreters DbSql interpretDbSql GetPersons" ]

main :: IO ()
main = hspec spec

