
{-# LANGUAGE FlexibleContexts #-}

module Harchi.FreeT.Effects.LoggerSpec where

import Control.Monad.Trans.Free (FreeT(..), FreeF(..))
import Control.Monad.Writer (Writer, MonadWriter, runWriter, tell)
import Test.Hspec

import Harchi.FreeT.Effects.Logger (LoggerF(..), LoggerT, MonadLogger, logMsg)

runLoggerWriter :: MonadWriter [String] m => LoggerT m r -> m r
runLoggerWriter app = do
  x0 <- runFreeT app
  case x0 of
    Pure r -> return r
    Free (LogMsg str next) -> do
      tell [str]
      runLoggerWriter (next ())

runApp :: LoggerT (Writer [String]) a -> (a, [String])
runApp = runWriter . runLoggerWriter

app1 :: (MonadLogger m, MonadWriter [String] m) => m ()
app1 = do
  logMsg "foo"
  logMsg "bar"

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Effects Logger" $ do
    it "foo bar" $ runApp app1 `shouldBe` ((), ["foo", "bar"])

