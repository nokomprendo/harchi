
{-# LANGUAGE FlexibleContexts #-}

module Harchi.FreeT.Effects.DbSpec where

import Control.Monad.Trans.Free (FreeT(..), FreeF(..))
import Control.Monad.Reader (MonadReader, Reader, runReader, ask)
import Test.Hspec

import Harchi.FreeT.Domain.Person (Person(..))
import Harchi.FreeT.Effects.Db (MonadDb, DbF(..), DbT, getPersons, getPersonFromId)

person1, person2 :: Person
person1 = Person 1 "Ada Lovelace" "GB"
person2 = Person 2 "Magaret Hamilton" "USA"

persons :: [Person]
persons = [person1, person2]

runDbReader :: MonadReader [Person] m => DbT m r -> m r
runDbReader app = do
  x0 <- runFreeT app
  case x0 of
    Pure r -> return r

    Free (GetPersons next) -> do
      ps <- ask
      runDbReader (next ps)

    Free (GetPersonFromId i next) -> do
      ps <- ask
      runDbReader (next $ filter ((==i) . personId) ps)

runApp :: DbT (Reader [Person]) a -> a
runApp app = runReader (runDbReader app) persons

app1 :: (MonadReader [Person] m, MonadDb m) => m [Person]
app1 = getPersons

app2 :: (MonadReader [Person] m, MonadDb m) => m [Person]
app2 = getPersonFromId 1

app3 :: (MonadReader [Person] m, MonadDb m) => m [Person]
app3 = getPersonFromId 42

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Effects Db" $ do
    it "getPersons" $ runApp app1 `shouldBe` persons
    it "getPersonFromId 1" $ runApp app2 `shouldBe` [person1]
    it "getPersonFromId 42" $ runApp app3 `shouldBe` []

