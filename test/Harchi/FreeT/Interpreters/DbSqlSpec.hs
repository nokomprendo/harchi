
{-# LANGUAGE FlexibleContexts #-}

module Harchi.FreeT.Interpreters.DbSqlSpec where

import Control.Monad.IO.Class (MonadIO)
import Control.Monad.Trans.Free (FreeT(..), FreeF(..))
import Control.Monad.Writer (MonadWriter, tell)
import Control.Monad.Trans.Writer (runWriterT, WriterT)
import Test.Hspec

import Harchi.FreeT.Domain.Person (Person(..))
import Harchi.FreeT.Effects.Db (DbT, getPersons, getPersonFromId, MonadDb)
import Harchi.FreeT.Effects.Logger (LoggerF(..), LoggerT, MonadLogger)
import Harchi.FreeT.Interpreters.DbSql (runDbSql)

sqlFile :: FilePath
sqlFile = "persons.db"

person1, person2 :: Person
person1 = Person 1 "Haskell Curry" "USA"
person2 = Person 2 "Alan Turing" "GB"

persons :: [Person]
persons = [person1, person2]

runLoggerWriter :: MonadWriter [String] m => LoggerT m r -> m r
runLoggerWriter app = do
  x0 <- runFreeT app
  case x0 of
    Pure r -> return r
    Free (LogMsg str next) -> do
      tell [str]
      runLoggerWriter (next ())

runApp :: MonadIO m => DbT (LoggerT (WriterT [String] m)) r -> m (r, [String])
runApp app = runWriterT $ runLoggerWriter $ runDbSql sqlFile app

app1 :: (MonadLogger m, MonadDb m) => m [Person]
app1 = getPersons

app2 :: (MonadLogger m, MonadDb m) => m [Person]
app2 = getPersonFromId 1

app3 :: (MonadLogger m, MonadDb m) => m [Person]
app3 = getPersonFromId 42

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Interpreters DbSql" $ do

    it "getPersons" $ do
      res <- runApp app1 
      res `shouldBe` (persons, ["Interpreters DbSql interpretDbSql GetPersons"])

    it "getPersonFromId 1" $ do
      res <- runApp app2 
      res `shouldBe` ([person1], ["Interpreters DbSql interpretDbSql GetPersonFromId 1"])

    it "getPersonFromId 42" $ do
      res <- runApp app3 
      res `shouldBe` ([], ["Interpreters DbSql interpretDbSql GetPersonFromId 42"])

