
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Harchi.ReaderT.Applications.ApiSpec where

import Control.Monad.Reader (ReaderT, runReaderT)
import Network.Wai (Application)
import Servant (Handler(..))
import Servant.Server.Generic (genericServeT)
import Test.Hspec
import Test.Hspec.Wai (with, get, shouldRespondWith)
import Test.Hspec.Wai.JSON (json)

import Harchi.ReaderT.Domain.Person (Person(..))
import Harchi.ReaderT.Interpreters.Env (Env(..))
import Harchi.ReaderT.Applications.Api (serverRecord)

sqlFile :: FilePath
sqlFile = "persons.db"

persons :: [Person]
persons = 
  [ Person 1 "Haskell Curry" "USA"
  , Person 2 "Alan Turing" "GB"
  ]

type AppM = ReaderT Env Handler

serverApp :: IO Application 
serverApp = 
  return $ genericServeT runApp serverRecord
    where
      env = Env
        { _logger = \_ -> pure ()
        , _sqlFile = sqlFile
        }
      runApp :: AppM a -> Handler a 
      runApp app = runReaderT app env

spec :: Spec
spec = do

  with serverApp $

    describe "Applications Api" $ do

      it "person 2" $ do
        get "/person/2" `shouldRespondWith` 
          [json|
            [ {personId: 2, personName: "Alan Turing", personCountry: "GB"}
            ]
          |]

      it "home" $ do
        get "/" `shouldRespondWith` "\"hello\""

      it "person 42" $ do
        get "/person/42" `shouldRespondWith` "[]"

      it "person 1" $ do
        get "/person/1" `shouldRespondWith` 
          "[{\"personId\":1,\"personName\":\"Haskell Curry\",\"personCountry\":\"USA\"}]"

      it "persons" $ do
        get "/persons" `shouldRespondWith` 
          [json|
            [ {personId: 1, personName: "Haskell Curry", personCountry: "USA"}
            , {personId: 2, personName: "Alan Turing", personCountry: "GB"}
            ]
          |]

main :: IO ()
main = hspec spec

