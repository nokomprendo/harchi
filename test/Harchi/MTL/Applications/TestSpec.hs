
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

{-# OPTIONS_GHC -Wno-orphans #-} 

module Harchi.MTL.Applications.TestSpec where

import Control.Monad.Writer (WriterT, tell, runWriterT)
import Test.Hspec

import Harchi.MTL.Domain.Person (Person(..))
import Harchi.MTL.Effects.Logger (MonadLogger, logMsg)
import Harchi.MTL.Interpreters.DbSql (runDbSqlT, DbSqlT(..))
import Harchi.MTL.Applications.Test (testApp)

sqlFile :: FilePath
sqlFile = "persons.db"

person1, person2 :: Person
person1 = Person 1 "Haskell Curry" "USA"
person2 = Person 2 "Alan Turing" "GB"

persons :: [Person]
persons = [person1, person2]

instance Monad m => MonadLogger (WriterT [String] m) where
  logMsg str = tell [str]

runTestApp :: DbSqlT (WriterT [String] IO) [Person] -> IO ([Person], [String])
runTestApp app = runWriterT $ runDbSqlT sqlFile app

spec :: Spec
spec = do

  describe "Applications Test" $ do

    it "logs + db" $ do
      (ps, logs) <- runTestApp testApp 
      ps `shouldBe` (person2 : persons)
      logs `shouldBe` 
        [ "Applications Test testApp getPerson 2"
        , "Interpreters DbSql getPersonFromId 2"
        , "Applications Test testApp getPersons"
        , "Interpreters DbSql getPersons" ]

main :: IO ()
main = hspec spec

