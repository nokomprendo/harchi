
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

{-# OPTIONS_GHC -Wno-orphans #-} 

module Harchi.MTL.Applications.ApiSpec where

import Control.Monad.Identity
import Network.Wai (Application)
import Servant (Handler(..))
import Servant.Server.Generic (genericServeT)
import Test.Hspec
import Test.Hspec.Wai (with, get, shouldRespondWith)
import Test.Hspec.Wai.JSON (json)

import Harchi.MTL.Domain.Person (Person(..))
import Harchi.MTL.Effects.Logger (MonadLogger, logMsg)
import Harchi.MTL.Interpreters.DbSql (DbSqlT, runDbSqlT)
import Harchi.MTL.Applications.Api (serverRecord)

sqlFile :: FilePath
sqlFile = "persons.db"

persons :: [Person]
persons = 
  [ Person 1 "Haskell Curry" "USA"
  , Person 2 "Alan Turing" "GB"
  ]

instance Monad m => MonadLogger (IdentityT m) where
  logMsg _str = pure ()

serverApp :: IO Application 
serverApp = 
  return $ genericServeT runApp serverRecord
    where
      runApp :: DbSqlT (IdentityT Handler) a -> Handler a
      runApp app = runIdentityT $ runDbSqlT sqlFile app

spec :: Spec
spec = do

  with serverApp $

    describe "Applications Api" $ do

      it "person 2" $ do
        get "/person/2" `shouldRespondWith` 
          [json|
            [ {personId: 2, personName: "Alan Turing", personCountry: "GB"}
            ]
          |]

      it "home" $ do
        get "/" `shouldRespondWith` "\"hello\""

      it "person 42" $ do
        get "/person/42" `shouldRespondWith` "[]"

      it "person 1" $ do
        get "/person/1" `shouldRespondWith` 
          "[{\"personId\":1,\"personName\":\"Haskell Curry\",\"personCountry\":\"USA\"}]"

      it "persons" $ do
        get "/persons" `shouldRespondWith` 
          [json|
            [ {personId: 1, personName: "Haskell Curry", personCountry: "USA"}
            , {personId: 2, personName: "Alan Turing", personCountry: "GB"}
            ]
          |]

main :: IO ()
main = hspec spec

