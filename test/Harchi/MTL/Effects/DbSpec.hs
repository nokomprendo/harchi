
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}

{-# OPTIONS_GHC -Wno-orphans #-} 

module Harchi.MTL.Effects.DbSpec where

import Control.Monad.Reader (Reader, runReader, ask, asks)
import Test.Hspec

import Harchi.MTL.Domain.Person (Person(..))
import Harchi.MTL.Effects.Db (MonadDb, getPersons, getPersonFromId)

person1, person2 :: Person
person1 = Person 1 "Ada Lovelace" "GB"
person2 = Person 2 "Magaret Hamilton" "USA"

persons :: [Person]
persons = [person1, person2]

instance MonadDb (Reader [Person]) where
  getPersons = ask
  getPersonFromId i = asks $ filter ((==i) . personId)

runApp :: Reader [Person] a -> a
runApp app = runReader app persons

app1 :: (MonadDb m) => m [Person]
app1 = getPersons

app2 :: (MonadDb m) => m [Person]
app2 = getPersonFromId 1

app3 :: (MonadDb m) => m [Person]
app3 = getPersonFromId 42

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Effects Db" $ do
    it "getPersons" $ runApp app1 `shouldBe` persons
    it "getPersonFromId 1" $ runApp app2 `shouldBe` [person1]
    it "getPersonFromId 42" $ runApp app3 `shouldBe` []

