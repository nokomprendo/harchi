
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}

{-# OPTIONS_GHC -Wno-orphans #-} 

module Harchi.MTL.Effects.LoggerSpec where

import Control.Monad.Writer (Writer, runWriter, tell)
import Test.Hspec

import Harchi.MTL.Effects.Logger (MonadLogger, logMsg)

instance MonadLogger (Writer [String]) where
  logMsg str = tell [str]

runApp :: Writer w a -> (a, w)
runApp = runWriter 

app1 :: (MonadLogger m) => m ()
app1 = do
  logMsg "foo"
  logMsg "bar"

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Effects Logger" $ do
    it "foo bar" $ runApp app1 `shouldBe` ((), ["foo", "bar"])

