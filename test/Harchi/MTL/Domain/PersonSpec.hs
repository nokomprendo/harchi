
module Harchi.MTL.Domain.PersonSpec where

import Test.Hspec

import Harchi.MTL.Domain.Person (Person(..))

person1 :: Person
person1 = Person 42 "name1" "country1"

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Domain Person" $ do
    it "person1 id" $ personId person1 `shouldBe` 42
    it "person1 name" $ personName person1 `shouldBe` "name1"
    it "person1 country" $ personCountry person1 `shouldBe` "country1"

