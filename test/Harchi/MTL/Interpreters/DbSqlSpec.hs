
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

{-# OPTIONS_GHC -Wno-orphans #-} 

module Harchi.MTL.Interpreters.DbSqlSpec where

import Control.Monad.Writer (listen, pass, writer, WriterT, MonadWriter, tell, lift, runWriterT)
import Test.Hspec

import Harchi.MTL.Domain.Person (Person(..))
import Harchi.MTL.Effects.Db (getPersons, getPersonFromId, MonadDb)
import Harchi.MTL.Effects.Logger (MonadLogger, logMsg)
import Harchi.MTL.Interpreters.DbSql (runDbSqlT, DbSqlT(..))

sqlFile :: FilePath
sqlFile = "persons.db"

person1, person2 :: Person
person1 = Person 1 "Haskell Curry" "USA"
person2 = Person 2 "Alan Turing" "GB"

persons :: [Person]
persons = [person1, person2]

instance Monad m => MonadLogger (WriterT [String] m) where
  logMsg str = tell [str]

instance Monad m => MonadWriter [String] (DbSqlT (WriterT [String] m)) where
  tell = lift . tell 
  writer = lift . writer
  listen = DbSqlT . listen . unDbSqlT
  pass = DbSqlT . pass . unDbSqlT
  
runApp :: DbSqlT (WriterT w m) a -> m (a, w)
runApp app = runWriterT $ runDbSqlT sqlFile app

app1 :: (MonadLogger m, MonadDb m) => m [Person]
app1 = getPersons

app2 :: (MonadLogger m, MonadDb m) => m [Person]
app2 = getPersonFromId 1

app3 :: (MonadLogger m, MonadDb m) => m [Person]
app3 = getPersonFromId 42

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Interpreters DbSql" $ do

    it "getPersons" $ do
      res <- runApp app1 
      res `shouldBe` (persons, ["Interpreters DbSql getPersons"])

    it "getPersonFromId 1" $ do
      res <- runApp app2 
      res `shouldBe` ([person1], ["Interpreters DbSql getPersonFromId 1"])

    it "getPersonFromId 42" $ do
      res <- runApp app3 
      res `shouldBe` ([], ["Interpreters DbSql getPersonFromId 42"])

