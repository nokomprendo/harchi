
{-# LANGUAGE LambdaCase #-}

module Harchi.DTALC.Effects.LoggerSpec where

import Control.Monad.Free 
import Control.Monad.Writer
import Test.Hspec

import Harchi.DTALC.Effects.Logger 
import Harchi.DTALC.Utils

-------------------------------------------------------------------------------

type WriterStrings = Writer [String]

runLoggerWriter :: (LoggerWriter f) => Free f a -> WriterStrings a
runLoggerWriter = myFoldFree pure loggerAlgebra 

class Functor f => LoggerWriter f where
  loggerAlgebra :: f (WriterStrings a) -> WriterStrings a

instance LoggerWriter LoggerF  where
  loggerAlgebra = \case
    LogMsg str next -> do
      tell [str]
      next

-------------------------------------------------------------------------------

runApp :: Free LoggerF a -> (a, [String])
runApp = runWriter . runLoggerWriter

app1 :: Free LoggerF ()
app1 = do
  logMsg "foo"
  logMsg "bar"

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Effects Logger" $ do
    it "foo bar" $ runApp app1 `shouldBe` ((), ["foo", "bar"])

