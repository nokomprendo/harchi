
{-# LANGUAGE LambdaCase #-}

module Harchi.DTALC.Effects.DbSpec where

import Control.Monad.Free 
import Control.Monad.Reader (Reader, runReader, ask)
import Test.Hspec

import Harchi.DTALC.Domain.Person (Person(..))
import Harchi.DTALC.Effects.Db 
import Harchi.DTALC.Utils

person1, person2 :: Person
person1 = Person 1 "Ada Lovelace" "GB"
person2 = Person 2 "Magaret Hamilton" "USA"

persons :: [Person]
persons = [person1, person2]

-------------------------------------------------------------------------------

type ReaderPersons = Reader [Person]

runDbReader :: (DbReader f) => Free f a -> ReaderPersons a
runDbReader = myFoldFree pure dbAlgebra 

class Functor f => DbReader f where
  dbAlgebra :: f (ReaderPersons a) -> ReaderPersons a

instance DbReader DbF  where
  dbAlgebra = \case
    GetPersons next -> 
      ask >>= next
    GetPersonFromId i next -> do
      ps <- ask
      next $ filter ((==i) . personId) ps

runApp :: Db a -> a
runApp app = runReader (runDbReader app) persons

type Db = Free DbF

app1 :: Db [Person]
app1 = getPersons

app2 :: Db [Person]
app2 = getPersonFromId 1

app3 :: Db [Person]
app3 = getPersonFromId 42

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Effects Db" $ do
    it "getPersons" $ runApp app1 `shouldBe` persons
    it "getPersonFromId 1" $ runApp app2 `shouldBe` [person1]
    it "getPersonFromId 42" $ runApp app3 `shouldBe` []


