
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}

module Harchi.DTALC.Applications.TestSpec where

import Control.Monad.Free 
import Control.Monad.Writer
import Test.Hspec

import Harchi.DTALC.Domain.Person (Person(..))
import Harchi.DTALC.Effects.Logger 
import Harchi.DTALC.Effects.Db 
import Harchi.DTALC.Interpreters.DbSql 
import Harchi.DTALC.Applications.Test 
import Harchi.DTALC.Utils

gSqlFile :: FilePath
gSqlFile = "persons.db"

person1, person2 :: Person
person1 = Person 1 "Haskell Curry" "USA"
person2 = Person 2 "Alan Turing" "GB"

persons :: [Person]
persons = [person1, person2]

-------------------------------------------------------------------------------

class Functor f => LoggerWriter f where
  loggerAlgebra :: (MonadWriter [String] m) => f (m a) -> m a

instance LoggerWriter LoggerF  where
  loggerAlgebra = \case
    LogMsg str next -> do
      tell [str]
      next

-------------------------------------------------------------------------------

class Functor f => DbLoggerWriter f where
  dlwAlgebra :: (MonadIO m, MonadWriter [String] m) => FilePath -> f (m a) -> m a

instance (LoggerWriter f, DbSql g) => DbLoggerWriter (f :+: g) where
  dlwAlgebra _sqlFile (InL r) = loggerAlgebra r
  dlwAlgebra sqlFile (InR r) = dbAlgebra sqlFile r

type App = Free (LoggerF :+: DbF)

runDLW :: (MonadIO m, MonadWriter [String] m) => FilePath -> App a -> m a
runDLW sqlFile = myFoldFree pure (dlwAlgebra sqlFile)

-------------------------------------------------------------------------------

runTestApp :: (MonadIO m) => App a -> m (a, [String])
runTestApp app = runWriterT (runDLW gSqlFile app)

spec :: Spec
spec = do

  describe "Applications Test" $ do

    it "logs + db" $ do
      (ps, logs) <- runTestApp testApp 
      ps `shouldBe` (person2 : persons)
      logs `shouldBe` 
        [ "Applications Test testApp getPerson 2"
        , "Applications Test testApp getPersons" ]

main :: IO ()
main = hspec spec


