
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Harchi.DTALC.Applications.ApiSpec where

import Control.Monad.IO.Class 
import Network.Wai (Application)
import Servant (Handler(..))
import Servant.Server.Generic (genericServeT)
import Test.Hspec
import Test.Hspec.Wai (with, get, shouldRespondWith)
import Test.Hspec.Wai.JSON (json)

import Harchi.DTALC.Domain.Person (Person(..))
import Harchi.DTALC.Effects.Logger 
import Harchi.DTALC.Interpreters.DbSql 
import Harchi.DTALC.Applications.Api 
import Harchi.DTALC.Applications.DbLogger 
import Harchi.DTALC.Utils

gSqlFile :: FilePath
gSqlFile = "persons.db"

persons :: [Person]
persons = 
  [ Person 1 "Haskell Curry" "USA"
  , Person 2 "Alan Turing" "GB"
  ]

-------------------------------------------------------------------------------

class Functor f => LoggerNull f where
  loggerAlgebra :: (Monad m) => f (m a) -> m a

instance LoggerNull LoggerF  where
  loggerAlgebra = \case
    LogMsg _str next -> next

-------------------------------------------------------------------------------

class Functor f => DbLoggerNull f where
  dlnAlgebra :: (MonadIO m) => FilePath -> f (m a) -> m a

instance (LoggerNull f, DbSql g) => DbLoggerNull (f :+: g) where
  dlnAlgebra _sqlFile (InL r) = loggerAlgebra r
  dlnAlgebra sqlFile (InR r) = dbAlgebra sqlFile r

-------------------------------------------------------------------------------

serverApp :: IO Application 
serverApp = 
  return $ genericServeT runApp serverRecord
    where
      -- runApp :: DbLogger Handler a -> Handler a 
      runApp :: App a -> Handler a
      runApp = myFoldFree pure (dlnAlgebra gSqlFile)

spec :: Spec
spec = do

  with serverApp $

    describe "Applications Api" $ do

      it "person 2" $ do
        get "/person/2" `shouldRespondWith` 
          [json|
            [ {personId: 2, personName: "Alan Turing", personCountry: "GB"}
            ]
          |]

      it "home" $ do
        get "/" `shouldRespondWith` "\"hello\""

      it "person 42" $ do
        get "/person/42" `shouldRespondWith` "[]"

      it "person 1" $ do
        get "/person/1" `shouldRespondWith` 
          "[{\"personId\":1,\"personName\":\"Haskell Curry\",\"personCountry\":\"USA\"}]"

      it "persons" $ do
        get "/persons" `shouldRespondWith` 
          [json|
            [ {personId: 1, personName: "Haskell Curry", personCountry: "USA"}
            , {personId: 2, personName: "Alan Turing", personCountry: "GB"}
            ]
          |]

main :: IO ()
main = hspec spec


