

module Harchi.DTALC.Interpreters.DbSqlSpec where

import Control.Monad.Free (Free)
import Test.Hspec

import Harchi.DTALC.Domain.Person (Person(..))
import Harchi.DTALC.Effects.Db 
import Harchi.DTALC.Interpreters.DbSql 

sqlFile :: FilePath
sqlFile = "persons.db"

person1, person2 :: Person
person1 = Person 1 "Haskell Curry" "USA"
person2 = Person 2 "Alan Turing" "GB"

persons :: [Person]
persons = [person1, person2]

type Db = Free DbF

app1 :: Db [Person]
app1 = getPersons

app2 :: Db [Person]
app2 = getPersonFromId 1

app3 :: Db [Person]
app3 = getPersonFromId 42

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Interpreters DbSql" $ do

    it "getPersons" $ do
      res <- runDbSql sqlFile app1 
      res `shouldBe` persons

    it "getPersonFromId 1" $ do
      res <- runDbSql sqlFile app2 
      res `shouldBe` [person1]

    it "getPersonFromId 42" $ do
      res <- runDbSql sqlFile app3 
      res `shouldBe` []

