
module Harchi.TLHC.TestSpec where

import Control.Concurrent.STM (TVar, readTVarIO, modifyTVar', newTVarIO, atomically)
import Test.Hspec

import Harchi.TLHC.Layer3.Person (Person(..))
import Harchi.TLHC.Layer1.AppT (AppT, runAppT, Env(..))
import Harchi.TLHC.Layer1.Test (testApp)

sqlFile :: FilePath
sqlFile = "persons.db"

person1, person2 :: Person
person1 = Person 1 "Haskell Curry" "USA"
person2 = Person 2 "Alan Turing" "GB"

persons :: [Person]
persons = [person1, person2]

runTestApp :: AppT IO [Person] -> TVar [String] -> IO [Person]
runTestApp app var = runAppT app env
  where
    env = Env
      { _logger = \str -> atomically $ modifyTVar' var (\strs -> strs ++ [str])
      , _sqlFile = sqlFile
      }

spec :: Spec
spec = do

  describe "Applications Test" $ do

    it "logs + db" $ do

      var <- newTVarIO []
      ps <- runTestApp testApp var
      ps `shouldBe` (person2 : persons)
      logs <- readTVarIO var
      logs `shouldBe` 
        [ "Layer1 Test testApp getPerson 2"
        , "Layer2 MonadPerson getPersonFromId 2"
        , "Layer1 Test testApp getPersons"
        , "Layer2 MonadPerson getPersons" ]

main :: IO ()
main = hspec spec

