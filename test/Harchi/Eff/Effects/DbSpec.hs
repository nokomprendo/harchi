
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}

module Harchi.Eff.Effects.DbSpec where

import Polysemy (Member, Sem, interpret, Members, run)
import Polysemy.Reader (Reader, runReader, ask)
import Test.Hspec

import Harchi.Eff.Domain.Person (Person(..))
import Harchi.Eff.Effects.Db (Db(..), getPersons, getPersonFromId)

person1, person2 :: Person
person1 = Person 1 "Ada Lovelace" "GB"
person2 = Person 2 "Magaret Hamilton" "USA"

persons :: [Person]
persons = [person1, person2]

runDbReader :: Member (Reader [Person]) r => Sem (Db ': r) a -> Sem r a
runDbReader = interpret \case
  GetPersons -> ask
  GetPersonFromId i -> filter ((==i) . personId) <$> ask

runApp :: Sem '[Db, Reader [Person]] a -> a
runApp = run . runReader persons . runDbReader

app1 :: Members '[Db] r => Sem r [Person]
app1 = getPersons

app2 :: Members '[Db] r => Sem r [Person]
app2 = getPersonFromId 1

app3 :: Members '[Db] r => Sem r [Person]
app3 = getPersonFromId 42

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Effects Db" $ do
    it "getPersons" $ runApp app1 `shouldBe` persons
    it "getPersonFromId 1" $ runApp app2 `shouldBe` [person1]
    it "getPersonFromId 42" $ runApp app3 `shouldBe` []

