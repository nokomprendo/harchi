
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}

module Harchi.Eff.Effects.LoggerSpec where

import Polysemy (Member, Members, Sem, interpret, run)
import Polysemy.Writer (Writer, runWriter, tell)
import Test.Hspec

import Harchi.Eff.Effects.Logger (Logger(..), logMsg)

runLoggerWriter :: Member (Writer [String]) r => Sem (Logger ': r) a -> Sem r a
runLoggerWriter = interpret \case
  LogMsg str -> tell [str]

runApp :: Sem '[Logger, Writer [String]] a -> ([String], a)
runApp = run . runWriter . runLoggerWriter

app1 :: Members '[Logger] r => Sem r ()
app1 = do
  logMsg "foo"
  logMsg "bar"

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Effects Logger" $ do
    it "foo bar" $ runApp app1 `shouldBe` (["foo", "bar"], ())

