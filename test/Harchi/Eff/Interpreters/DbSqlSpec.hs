
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}

module Harchi.Eff.Interpreters.DbSqlSpec where

import Polysemy (Member, Members, Sem, runM, Embed, interpret)
import Polysemy.Writer (Writer, runWriter, tell)
import Test.Hspec

import Harchi.Eff.Domain.Person (Person(..))
import Harchi.Eff.Effects.Db (Db, getPersons, getPersonFromId)
import Harchi.Eff.Effects.Logger (Logger(..))
import Harchi.Eff.Interpreters.DbSql (runDbSql)

sqlFile :: FilePath
sqlFile = "persons.db"

person1, person2 :: Person
person1 = Person 1 "Haskell Curry" "USA"
person2 = Person 2 "Alan Turing" "GB"

persons :: [Person]
persons = [person1, person2]

runLoggerWriter :: Member (Writer [String]) r => Sem (Logger ': r) a -> Sem r a
runLoggerWriter = interpret \case
  LogMsg str -> tell [str]

runApp :: Sem '[Db, Logger, Writer [String], Embed IO] a -> IO ([String], a)
runApp = runM . runWriter . runLoggerWriter . runDbSql sqlFile

app1 :: Members '[Db] r => Sem r [Person]
app1 = getPersons

app2 :: Members '[Db] r => Sem r [Person]
app2 = getPersonFromId 1

app3 :: Members '[Db] r => Sem r [Person]
app3 = getPersonFromId 42

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Interpreters DbSql" $ do

    it "getPersons" $ do
      res <- runApp app1 
      res `shouldBe` (["Interpreters DbSql runDbSql GetPersons"], persons)

    it "getPersonFromId 1" $ do
      res <- runApp app2 
      res `shouldBe` (["Interpreters DbSql runDbSql GetPersonFromId 1" ], [person1])

    it "getPersonFromId 42" $ do
      res <- runApp app3 
      res `shouldBe` (["Interpreters DbSql runDbSql GetPersonFromId 42" ], [])

