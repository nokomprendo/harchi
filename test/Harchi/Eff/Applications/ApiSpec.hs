
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Harchi.Eff.Applications.ApiSpec where

import Control.Monad.Trans.Except (ExceptT(..))
import Network.Wai (Application)
import Polysemy (Sem, Embed, interpret, runM)
import Servant (Handler(..))
import Servant.Server.Generic (genericServeT)
import Test.Hspec
import Test.Hspec.Wai (with, get, shouldRespondWith)
import Test.Hspec.Wai.JSON (json)

import Harchi.Eff.Domain.Person (Person(..))
import Harchi.Eff.Effects.Db (Db)
import Harchi.Eff.Effects.Logger (Logger(..))
import Harchi.Eff.Interpreters.DbSql (runDbSql)
import Harchi.Eff.Applications.Api (serverRecord)

sqlFile :: FilePath
sqlFile = "persons.db"

persons :: [Person]
persons = 
  [ Person 1 "Haskell Curry" "USA"
  , Person 2 "Alan Turing" "GB"
  ]

runLoggerNull :: Sem (Logger ': r) a -> Sem r a
runLoggerNull = interpret \case
  LogMsg _ -> pure ()

serverApp :: IO Application
serverApp = 
  return $ genericServeT runApp serverRecord

  where
    liftToHandler :: IO a -> Handler a
    liftToHandler = Handler . ExceptT . fmap Right

    runApp :: Sem '[Db, Logger, Embed IO] a -> Handler a
    runApp = liftToHandler . runM . runLoggerNull . runDbSql sqlFile

spec :: Spec
spec = do

  with serverApp $

    describe "Applications Api" $ do

      it "person 2" $ do
        get "/person/2" `shouldRespondWith` 
          [json|
            [ {personId: 2, personName: "Alan Turing", personCountry: "GB"}
            ]
          |]

      it "home" $ do
        get "/" `shouldRespondWith` "\"hello\""

      it "person 42" $ do
        get "/person/42" `shouldRespondWith` "[]"

      it "person 1" $ do
        get "/person/1" `shouldRespondWith` 
          "[{\"personId\":1,\"personName\":\"Haskell Curry\",\"personCountry\":\"USA\"}]"

      it "persons" $ do
        get "/persons" `shouldRespondWith` 
          [json|
            [ {personId: 1, personName: "Haskell Curry", personCountry: "USA"}
            , {personId: 2, personName: "Alan Turing", personCountry: "GB"}
            ]
          |]

main :: IO ()
main = hspec spec

