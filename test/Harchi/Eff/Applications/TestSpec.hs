
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}

module Harchi.Eff.Applications.TestSpec where

import Polysemy (Member, Sem, runM, Embed, interpret)
import Polysemy.Writer (Writer, runWriter, tell)
import Test.Hspec

import Harchi.Eff.Domain.Person (Person(..))
import Harchi.Eff.Effects.Db (Db)
import Harchi.Eff.Effects.Logger (Logger(..))
import Harchi.Eff.Interpreters.DbSql (runDbSql)
import Harchi.Eff.Applications.Test (testApp)

sqlFile :: FilePath
sqlFile = "persons.db"

person1, person2 :: Person
person1 = Person 1 "Haskell Curry" "USA"
person2 = Person 2 "Alan Turing" "GB"

persons :: [Person]
persons = [person1, person2]

runLoggerWriter :: Member (Writer [String]) r => Sem (Logger ': r) a -> Sem r a
runLoggerWriter = interpret \case
  LogMsg str -> tell [str]

runTestApp :: 
  Sem '[Db, Logger, Writer [String], Embed IO] [Person] ->
  IO ([String], [Person])
runTestApp = runM . runWriter . runLoggerWriter . runDbSql sqlFile

spec :: Spec
spec = do

  describe "Applications Test" $ do

    it "logs + db" $ do
      (logs, ps) <- runTestApp testApp 
      ps `shouldBe` (person2 : persons)
      logs `shouldBe` 
        [ "Applications Test testApp getPerson 2"
        , "Interpreters DbSql runDbSql GetPersonFromId 2"
        , "Applications Test testApp getPersons"
        , "Interpreters DbSql runDbSql GetPersons"]

main :: IO ()
main = hspec spec

