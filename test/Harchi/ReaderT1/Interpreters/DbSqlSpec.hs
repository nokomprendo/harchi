
module Harchi.ReaderT1.Interpreters.DbSqlSpec where

import Control.Concurrent.STM (TVar, readTVarIO, modifyTVar', newTVarIO, atomically)
import Control.Monad.Reader (ReaderT, runReaderT)
import Test.Hspec

import Harchi.ReaderT1.Domain.Person (Person(..))
import Harchi.ReaderT1.Effects.Db (getPersons, getPersonFromId, HasDb, getDbPersons, getDbPersonFromId)
import Harchi.ReaderT1.Effects.Logger (HasLogger, getLogger, logMsg)
import Harchi.ReaderT1.Interpreters.DbSql (getPersonsSql, getPersonFromIdSql)

sqlFile :: FilePath
sqlFile = "persons.db"

person1, person2 :: Person
person1 = Person 1 "Haskell Curry" "USA"
person2 = Person 2 "Alan Turing" "GB"

persons :: [Person]
persons = [person1, person2]

data Env = Env
  { _logVar :: TVar [String]
  , _sqlFile :: FilePath
  }

type AppM = ReaderT Env IO

instance HasLogger Env where
  getLogger env str = atomically $ modifyTVar' (_logVar env) (\logs -> logs <> [str]) 

instance HasDb Env where
  getDbPersons = getPersonsSql . _sqlFile
  getDbPersonFromId env = getPersonFromIdSql (_sqlFile env)

runApp :: AppM a -> TVar [String] -> IO a
runApp app var = runReaderT app (Env var sqlFile)

app1 :: AppM [Person]
app1 = do
  logMsg "app1 getPersons"
  getPersons

app2 :: AppM [Person]
app2 = do
  logMsg "app2 getPersonFromId 1"
  getPersonFromId 1

app3 :: AppM [Person]
app3 = do
  logMsg "app3 getPersonFromId 42"
  getPersonFromId 42

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Interpreters DbSql" $ do

    it "getPersons" $ do
      var1 <- newTVarIO []
      res1 <- runApp app1 var1
      res1 `shouldBe` persons
      logs1 <- readTVarIO var1
      logs1 `shouldBe` ["app1 getPersons"]

    it "getPersonFromId 1" $ do
      var2 <- newTVarIO []
      res2 <- runApp app2 var2
      res2 `shouldBe` [person1]
      logs2 <- readTVarIO var2
      logs2 `shouldBe` ["app2 getPersonFromId 1"]

    it "getPersonFromId 42" $ do
      var3 <- newTVarIO []
      res3 <- runApp app3 var3
      res3 `shouldBe` []
      logs3 <- readTVarIO var3
      logs3 `shouldBe` ["app3 getPersonFromId 42"]

