
module Harchi.ReaderT1.Applications.TestSpec where

import Control.Concurrent.STM (TVar, readTVarIO, modifyTVar', newTVarIO, atomically)
import Control.Monad.Reader (ReaderT, runReaderT)
import Test.Hspec

import Harchi.ReaderT1.Domain.Person (Person(..))
import Harchi.ReaderT1.Effects.Db (HasDb, getDbPersons, getDbPersonFromId)
import Harchi.ReaderT1.Effects.Logger (HasLogger, getLogger)
import Harchi.ReaderT1.Interpreters.DbSql (getPersonsSql, getPersonFromIdSql)
import Harchi.ReaderT1.Applications.Test (testApp)

sqlFile :: FilePath
sqlFile = "persons.db"

person1, person2 :: Person
person1 = Person 1 "Haskell Curry" "USA"
person2 = Person 2 "Alan Turing" "GB"

persons :: [Person]
persons = [person1, person2]

data Env = Env
  { _logVar :: TVar [String]
  , _sqlFile :: FilePath
  }

type AppM = ReaderT Env IO

instance HasLogger Env where
  getLogger env str = atomically $ modifyTVar' (_logVar env) (\logs -> logs <> [str]) 

instance HasDb Env where
  getDbPersons = getPersonsSql . _sqlFile
  getDbPersonFromId env = getPersonFromIdSql (_sqlFile env)

runTestApp :: AppM [Person] -> TVar [String] -> IO [Person]
runTestApp app var = runReaderT app (Env var sqlFile)

spec :: Spec
spec = do

  describe "Applications Test" $ do

    it "logs + db" $ do

      var <- newTVarIO []
      ps <- runTestApp testApp var
      ps `shouldBe` (person2 : persons)
      logs <- readTVarIO var
      logs `shouldBe` 
        [ "Applications Test testApp getPerson 2"
        , "Applications Test testApp getPersons" ]

main :: IO ()
main = hspec spec

