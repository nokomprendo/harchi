
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

{-# OPTIONS_GHC -Wno-orphans #-} 

module Harchi.ReaderT1.Applications.ApiSpec where

import Control.Monad.Reader (ReaderT, runReaderT)
import Network.Wai (Application)
import Servant (Handler(..))
import Servant.Server.Generic (genericServeT)
import Test.Hspec
import Test.Hspec.Wai (with, get, shouldRespondWith)
import Test.Hspec.Wai.JSON (json)

import Harchi.ReaderT1.Domain.Person (Person(..))
import Harchi.ReaderT1.Effects.Logger (HasLogger, getLogger)
import Harchi.ReaderT1.Effects.Db (HasDb, getDbPersons, getDbPersonFromId)
import Harchi.ReaderT1.Interpreters.DbSql (getPersonsSql, getPersonFromIdSql)
import Harchi.ReaderT1.Applications.Api (serverRecord)

sqlFile :: FilePath
sqlFile = "persons.db"

persons :: [Person]
persons = 
  [ Person 1 "Haskell Curry" "USA"
  , Person 2 "Alan Turing" "GB"
  ]

newtype Env = Env
  { _sqlFile :: FilePath
  }

type AppM = ReaderT Env Handler

instance HasLogger Env where
  getLogger _env _str = pure ()

instance HasDb Env where
  getDbPersons = getPersonsSql . _sqlFile
  getDbPersonFromId env = getPersonFromIdSql (_sqlFile env)

serverApp :: IO Application 
serverApp = 
  return $ genericServeT runApp serverRecord
    where
      runApp :: AppM a -> Handler a 
      runApp app = runReaderT app (Env sqlFile)

spec :: Spec
spec = do

  with serverApp $

    describe "Applications Api" $ do

      it "person 2" $ do
        get "/person/2" `shouldRespondWith` 
          [json|
            [ {personId: 2, personName: "Alan Turing", personCountry: "GB"}
            ]
          |]

      it "home" $ do
        get "/" `shouldRespondWith` "\"hello\""

      it "person 42" $ do
        get "/person/42" `shouldRespondWith` "[]"

      it "person 1" $ do
        get "/person/1" `shouldRespondWith` 
          "[{\"personId\":1,\"personName\":\"Haskell Curry\",\"personCountry\":\"USA\"}]"

      it "persons" $ do
        get "/persons" `shouldRespondWith` 
          [json|
            [ {personId: 1, personName: "Haskell Curry", personCountry: "USA"}
            , {personId: 2, personName: "Alan Turing", personCountry: "GB"}
            ]
          |]

main :: IO ()
main = hspec spec

