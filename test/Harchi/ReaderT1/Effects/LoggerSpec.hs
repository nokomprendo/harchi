
module Harchi.ReaderT1.Effects.LoggerSpec where

import Control.Concurrent.STM (TVar, readTVarIO, modifyTVar', newTVarIO, atomically)
import Control.Monad.Reader (ReaderT, runReaderT)
import Test.Hspec

import Harchi.ReaderT1.Effects.Logger (HasLogger, getLogger, logMsg)

newtype Env = Env
  { _logVar :: TVar [String]
  }

type AppM = ReaderT Env IO

instance HasLogger Env where
  getLogger env str = atomically $ modifyTVar' (_logVar env) (\logs -> logs <> [str]) 

runApp :: AppM a -> TVar [String] -> IO a
runApp app var = runReaderT app $ Env var

app1 :: AppM ()
app1 = do
  logMsg "foo"
  logMsg "bar"

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Effects Logger" $ do
    it "foo bar" $ do
      var1 <- newTVarIO []
      runApp app1 var1
      res1 <- readTVarIO var1
      res1 `shouldBe` ["foo", "bar"]

