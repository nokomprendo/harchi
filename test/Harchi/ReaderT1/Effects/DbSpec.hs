
module Harchi.ReaderT1.Effects.DbSpec where

import Control.Monad.Reader (ReaderT, runReaderT)
import Test.Hspec

import Harchi.ReaderT1.Domain.Person (Person(..))
import Harchi.ReaderT1.Effects.Db (HasDb, getDbPersons, getDbPersonFromId, getPersons, getPersonFromId)

person1, person2 :: Person
person1 = Person 1 "Ada Lovelace" "GB"
person2 = Person 2 "Magaret Hamilton" "USA"

persons :: [Person]
persons = [person1, person2]

newtype Env = Env
  { _persons :: [Person]
  }

type AppM = ReaderT Env IO

instance HasDb Env where
  getDbPersons = pure . _persons
  getDbPersonFromId env i = pure $ filter ((==i) . personId) $ _persons env

runApp :: AppM a -> IO a
runApp app = runReaderT app (Env persons)

app1 ::AppM [Person]
app1 = getPersons

app2 ::AppM [Person]
app2 = getPersonFromId 1

app3 ::AppM [Person]
app3 = getPersonFromId 42

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Effects Db" $ do
    it "getPersons" $ do
      res1 <- runApp app1 
      res1 `shouldBe` persons

    it "getPersonFromId 1" $ do
      res2 <- runApp app2 
      res2 `shouldBe` [person1]

    it "getPersonFromId 42" $ do
      res3 <- runApp app3 
      res3 `shouldBe` []

