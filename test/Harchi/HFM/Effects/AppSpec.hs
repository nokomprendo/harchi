
{-# LANGUAGE LambdaCase #-}

module Harchi.HFM.Effects.AppSpec where

import Control.Monad.Free (foldFree)
import Control.Monad.State (State, gets, modify', runState)
import Test.Hspec

import Harchi.HFM.Domain.Person (Person(..))
import Harchi.HFM.Effects.App (App, AppF(..), appLogger, appDb)
import Harchi.HFM.Effects.Db (DbF(..), Db, getPersons, getPersonFromId)
import Harchi.HFM.Effects.Logger (LoggerF(..), Logger, logMsg)

person1, person2 :: Person
person1 = Person 1 "Ada Lovelace" "GB"
person2 = Person 2 "Magaret Hamilton" "USA"

persons :: [Person]
persons = [person1, person2]

data AppState = AppState
  { _persons :: [Person]
  , _logs :: [String]
  } deriving (Show, Eq)

interpretDbState :: DbF a -> State AppState a
interpretDbState = \case
  (GetPersons next) -> do
    ps <- gets _persons
    pure $ next ps
  (GetPersonFromId i next) -> do
    ps <- gets _persons
    pure . next $ filter ((==i) . personId) ps

runDbState :: Db a -> State AppState a
runDbState = foldFree interpretDbState 

interpretLoggerState :: LoggerF a -> State AppState a
interpretLoggerState (LogMsg str next) = do
  modify' $ \s -> s { _logs = _logs s ++ [str]}
  pure $ next ()

runLoggerState :: Logger a -> State AppState a
runLoggerState = foldFree interpretLoggerState

runAppF :: AppF a -> State AppState a
runAppF = \case
  (AppLogger action next) -> next <$> runLoggerState action 
  (AppDb action next)     -> next <$> runDbState action 
  -- (AppIO _action _next)   -> error "cannot run IO"

runApp :: App a -> (a, AppState)
runApp app = runState (foldFree runAppF app) (AppState persons [])

app1 :: App ()
app1 = do
  appLogger $ logMsg "foo"
  appLogger $ logMsg "bar"

app2 :: App [Person]
app2 = do
  appLogger $ logMsg "bar"
  ps <- appDb $ getPersonFromId 1
  appLogger $ logMsg "foo"
  return ps

app3 :: App [Person]
app3 = appDb $ getPersonFromId 42

app4 :: App [Person]
app4 = appDb getPersons

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Effects App" $ do
    it "app1" $ runApp app1 `shouldBe` ((), AppState persons ["foo", "bar"])
    it "app2" $ runApp app2 `shouldBe` ([person1], AppState persons ["bar", "foo"])
    it "app3" $ runApp app3 `shouldBe` ([], AppState persons [])
    it "app4" $ runApp app4 `shouldBe` (persons, AppState persons [])


