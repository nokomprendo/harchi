
module Harchi.HFM.Effects.LoggerSpec where

import Control.Monad.Free (foldFree)
import Control.Monad.Writer (Writer, runWriter, tell)
import Test.Hspec

import Harchi.HFM.Effects.Logger (LoggerF(..), Logger, logMsg)

interpretLoggerWriter :: LoggerF a -> Writer [String] a
interpretLoggerWriter (LogMsg str next) = do
  tell [str]
  pure $ next ()

runApp :: Logger a -> (a, [String])
runApp = runWriter . foldFree interpretLoggerWriter

app1 :: Logger ()
app1 = do
  logMsg "foo"
  logMsg "bar"

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Effects Logger" $ do
    it "foo bar" $ runApp app1 `shouldBe` ((), ["foo", "bar"])

