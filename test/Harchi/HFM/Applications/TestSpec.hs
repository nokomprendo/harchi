
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE FlexibleContexts #-}

module Harchi.HFM.Applications.TestSpec where

import Control.Monad.Free (foldFree)
-- import Control.Monad.IO.Class (MonadIO)
import Control.Monad.Writer (WriterT, runWriterT, tell)
import Test.Hspec

import Harchi.HFM.Domain.Person (Person(..))
import Harchi.HFM.Effects.App (App, AppF(..))
import Harchi.HFM.Effects.Logger -- (Logger, LoggerF(..))
import Harchi.HFM.Interpreters.DbSql (runDbSql)
import Harchi.HFM.Applications.Test (testApp)

sqlFile :: FilePath
sqlFile = "persons.db"

person1, person2 :: Person
person1 = Person 1 "Haskell Curry" "USA"
person2 = Person 2 "Alan Turing" "GB"

persons :: [Person]
persons = [person1, person2]


-- interpretLoggerWriter :: MonadWriter [String] m => LoggerF a -> m a
interpretLoggerWriter :: LoggerF a -> WriterT [String] IO a 
interpretLoggerWriter (LogMsg str next) = do
  tell [str]
  pure $ next ()

runLoggerWriter :: Logger a -> WriterT [String] IO a
runLoggerWriter = foldFree interpretLoggerWriter

-- interpretApp :: (MonadWriter [String] m, MonadIO m) => AppF a -> m a
interpretApp :: AppF a -> WriterT [String] IO a
interpretApp = \case
  AppLogger action next -> next <$> runLoggerWriter action 
  AppDb action next     -> next <$> runDbSql sqlFile action 

runTestApp :: App [Person] -> IO ([Person], [String])
runTestApp = runWriterT . foldFree interpretApp 

spec :: Spec
spec = do

  describe "Applications Test" $ do

    it "logs + db" $ do
      (ps, logs) <- runTestApp testApp 
      ps `shouldBe` (person2 : persons)
      logs `shouldBe` 
        [ "Applications Test testApp getPerson 2"
        , "Applications Test testApp getPersons" ]

main :: IO ()
main = hspec spec

