
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Harchi.HFM.Applications.ApiSpec where

-- import Control.Monad.IO.Class (liftIO)
import Control.Monad.Free (foldFree)
import Network.Wai (Application)
import Servant (Handler(..))
import Servant.Server.Generic (genericServeT)
import Test.Hspec
import Test.Hspec.Wai (with, get, shouldRespondWith)
import Test.Hspec.Wai.JSON (json)

import Harchi.HFM.Domain.Person (Person(..))
import Harchi.HFM.Effects.App (App, AppF(..))
import Harchi.HFM.Effects.Logger (LoggerF(..), Logger)
import Harchi.HFM.Interpreters.DbSql (runDbSql)
import Harchi.HFM.Applications.Api (serverRecord)

sqlFile :: FilePath
sqlFile = "persons.db"

persons :: [Person]
persons = 
  [ Person 1 "Haskell Curry" "USA"
  , Person 2 "Alan Turing" "GB"
  ]

interpretLoggerNull :: Monad m => LoggerF a -> m a
interpretLoggerNull (LogMsg _str next) = pure $ next ()

runLoggerNull :: Logger a -> Handler a
runLoggerNull = foldFree interpretLoggerNull

runAppF :: AppF a -> Handler a
runAppF = \case
  (AppLogger action next) -> next <$> runLoggerNull action 
  (AppDb action next)     -> next <$> runDbSql sqlFile action 
  -- (AppIO action next)     -> next <$> liftIO action

runApp :: App a -> Handler a
runApp = foldFree runAppF 

serverApp :: IO Application 
serverApp = return $ genericServeT runApp serverRecord

spec :: Spec
spec = do

  with serverApp $

    describe "Applications Api" $ do

      it "person 2" $ do
        get "/person/2" `shouldRespondWith` 
          [json|
            [ {personId: 2, personName: "Alan Turing", personCountry: "GB"}
            ]
          |]

      it "home" $ do
        get "/" `shouldRespondWith` "\"hello\""

      it "person 42" $ do
        get "/person/42" `shouldRespondWith` "[]"

      it "person 1" $ do
        get "/person/1" `shouldRespondWith` 
          "[{\"personId\":1,\"personName\":\"Haskell Curry\",\"personCountry\":\"USA\"}]"

      it "persons" $ do
        get "/persons" `shouldRespondWith` 
          [json|
            [ {personId: 1, personName: "Haskell Curry", personCountry: "USA"}
            , {personId: 2, personName: "Alan Turing", personCountry: "GB"}
            ]
          |]

main :: IO ()
main = hspec spec

