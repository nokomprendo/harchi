
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Harchi.TLHC1.Applications.ApiSpec where

import Control.Monad.Reader (ReaderT)
import Network.Wai (Application)
import Servant (Handler(..))
import Servant.Server.Generic (genericServeT)
import Test.Hspec
import Test.Hspec.Wai (with, get, shouldRespondWith)
import Test.Hspec.Wai.JSON (json)

import Harchi.TLHC1.Domain.Person (Person(..))
import Harchi.TLHC1.Interpreters.AppT (AppT, Env(..), runAppT)
import Harchi.TLHC1.Applications.Api (serverRecord)

sqlFile :: FilePath
sqlFile = "persons.db"

persons :: [Person]
persons = 
  [ Person 1 "Haskell Curry" "USA"
  , Person 2 "Alan Turing" "GB"
  ]

type AppM = ReaderT Env Handler

serverApp :: IO Application 
serverApp = 
  return $ genericServeT runApp serverRecord
    where
      env = Env
        { _logger = \_ -> pure ()
        , _sqlFile = sqlFile
        }
      runApp :: AppT Handler a -> Handler a 
      runApp app = runAppT app env

spec :: Spec
spec = do

  with serverApp $

    describe "Applications Api" $ do

      it "person 2" $ do
        get "/person/2" `shouldRespondWith` 
          [json|
            [ {personId: 2, personName: "Alan Turing", personCountry: "GB"}
            ]
          |]

      it "home" $ do
        get "/" `shouldRespondWith` "\"hello\""

      it "person 42" $ do
        get "/person/42" `shouldRespondWith` "[]"

      it "person 1" $ do
        get "/person/1" `shouldRespondWith` 
          "[{\"personId\":1,\"personName\":\"Haskell Curry\",\"personCountry\":\"USA\"}]"

      it "persons" $ do
        get "/persons" `shouldRespondWith` 
          [json|
            [ {personId: 1, personName: "Haskell Curry", personCountry: "USA"}
            , {personId: 2, personName: "Alan Turing", personCountry: "GB"}
            ]
          |]

main :: IO ()
main = hspec spec

