
module Harchi.TLHC1.Applications.TestSpec where

import Control.Concurrent.STM (TVar, readTVarIO, modifyTVar', newTVarIO, atomically)
import Test.Hspec

import Harchi.TLHC1.Domain.Person (Person(..))
import Harchi.TLHC1.Interpreters.AppT (AppT, runAppT, Env(..))
import Harchi.TLHC1.Applications.Test (testApp)

sqlFile :: FilePath
sqlFile = "persons.db"

person1, person2 :: Person
person1 = Person 1 "Haskell Curry" "USA"
person2 = Person 2 "Alan Turing" "GB"

persons :: [Person]
persons = [person1, person2]

runTestApp :: AppT IO [Person] -> TVar [String] -> IO [Person]
runTestApp app var = runAppT app env
  where
    env = Env
      { _logger = \str -> atomically $ modifyTVar' var (\strs -> strs ++ [str])
      , _sqlFile = sqlFile
      }

spec :: Spec
spec = do

  describe "Applications Test" $ do

    it "logs + db" $ do

      var <- newTVarIO []
      ps <- runTestApp testApp var
      ps `shouldBe` (person2 : persons)
      logs <- readTVarIO var
      logs `shouldBe` 
        [ "Applications Test testApp getPerson 2"
        , "Interpreters AppT getPersonFromId 2"
        , "Applications Test testApp getPersons"
        , "Interpreters AppT getPersons" ]

main :: IO ()
main = hspec spec

