
{-# LANGUAGE FlexibleInstances #-}

module Harchi.TLHC1.Effects.MonadLoggerSpec where

import Control.Concurrent.STM (TVar, readTVarIO, modifyTVar', newTVarIO, atomically)
import Control.Monad.Reader (liftIO, asks, ReaderT, runReaderT)
import Test.Hspec

import Harchi.TLHC1.Effects.MonadLogger (MonadLogger, logMsg)

newtype Env = Env
  { _logVar :: TVar [String]
  }

type AppM = ReaderT Env IO

instance MonadLogger AppM where
  logMsg str = do
    var <- asks _logVar
    liftIO $ atomically $ modifyTVar' var (\logs -> logs <> [str]) 

runApp :: AppM a -> TVar [String] -> IO a
runApp app var = runReaderT app $ Env var

app1 :: AppM ()
app1 = do
  logMsg "foo"
  logMsg "bar"

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Effects MonadLogger" $ do
    it "foo bar" $ do
      var1 <- newTVarIO []
      runApp app1 var1
      res1 <- readTVarIO var1
      res1 `shouldBe` ["foo", "bar"]

