
{-# LANGUAGE FlexibleInstances #-}

module Harchi.TLHC1.Effects.MonadPersonSpec where

import Control.Monad.Reader (asks, ReaderT, runReaderT)
import Test.Hspec

import Harchi.TLHC1.Domain.Person (Person(..))
import Harchi.TLHC1.Effects.MonadPerson 

person1, person2 :: Person
person1 = Person 1 "Ada Lovelace" "GB"
person2 = Person 2 "Magaret Hamilton" "USA"

persons :: [Person]
persons = [person1, person2]

newtype Env = Env
  { _persons :: [Person]
  }

type AppM = ReaderT Env IO

instance MonadPerson AppM where
  getPersons = asks _persons
  getPersonFromId i = asks (filter ((==i) . personId) . _persons)

runApp :: AppM a -> IO a
runApp app = runReaderT app (Env persons)

app1 ::AppM [Person]
app1 = getPersons

app2 ::AppM [Person]
app2 = getPersonFromId 1

app3 ::AppM [Person]
app3 = getPersonFromId 42

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Effects Db" $ do
    it "getPersons" $ do
      res1 <- runApp app1 
      res1 `shouldBe` persons

    it "getPersonFromId 1" $ do
      res2 <- runApp app2 
      res2 `shouldBe` [person1]

    it "getPersonFromId 42" $ do
      res3 <- runApp app3 
      res3 `shouldBe` []

