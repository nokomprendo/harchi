
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Harchi.Func.Applications.ApiSpec where

import Network.Wai (Application)
import Servant.Server.Generic (genericServe)
import Test.Hspec
import Test.Hspec.Wai (with, get, shouldRespondWith)
import Test.Hspec.Wai.JSON (json)

import Harchi.Func.Domain.Person (Person(..))
import Harchi.Func.Interpreters.DbSql (getPersonsSql, getPersonFromIdSql)
import Harchi.Func.Applications.Api (serverRecord)

sqlFile :: FilePath
sqlFile = "persons.db"

serverApp :: IO Application 
serverApp = 
  return $ genericServe (serverRecord _logMsg _getPersons _getPersonFromId)
    where
      _logMsg _str = pure ()
      _getPersons = getPersonsSql sqlFile _logMsg
      _getPersonFromId = getPersonFromIdSql sqlFile _logMsg

persons :: [Person]
persons = 
  [ Person 1 "Haskell Curry" "USA"
  , Person 2 "Alan Turing" "GB"
  ]

spec :: Spec
spec = do

  with serverApp $

    describe "Applications Api" $ do

      it "person 2" $ do
        get "/person/2" `shouldRespondWith` 
          [json|
            [ {personId: 2, personName: "Alan Turing", personCountry: "GB"}
            ]
          |]

      it "persons" $ do
        get "/persons" `shouldRespondWith` 
          [json|
            [ {personId: 1, personName: "Haskell Curry", personCountry: "USA"}
            , {personId: 2, personName: "Alan Turing", personCountry: "GB"}
            ]
          |]

      it "person 42" $ do
        get "/person/42" `shouldRespondWith` "[]"

      it "person 1" $ do
        get "/person/1" `shouldRespondWith` 
          "[{\"personId\":1,\"personName\":\"Haskell Curry\",\"personCountry\":\"USA\"}]"

      it "home" $ do
        get "/" `shouldRespondWith` "\"hello\""

main :: IO ()
main = hspec spec

