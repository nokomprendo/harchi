
{-# LANGUAGE FlexibleContexts #-}

module Harchi.Func.Applications.TestSpec where

import Control.Monad.Writer (WriterT, tell, runWriterT)
import Test.Hspec

import Harchi.Func.Domain.Person (Person(..))
import Harchi.Func.Interpreters.DbSql (getPersonsSql, getPersonFromIdSql)
import Harchi.Func.Applications.Test (testApp, TestApp)

sqlFile :: FilePath
sqlFile = "persons.db"

type WriterIO = WriterT [String] IO

runTestApp :: TestApp WriterIO [Person] -> IO ([Person], [String])
runTestApp app = runWriterT (app _logMsg _getPersons _getPersonFromdId)
  where
    _logMsg str = tell [str]
    _getPersons = getPersonsSql sqlFile _logMsg
    _getPersonFromdId = getPersonFromIdSql sqlFile _logMsg

person1, person2 :: Person
person1 = Person 1 "Haskell Curry" "USA"
person2 = Person 2 "Alan Turing" "GB"

persons :: [Person]
persons = [person1, person2]

spec :: Spec
spec = do

  describe "Applications Test" $ do

    it "logs + db" $ do
      (ps, logs) <- runTestApp testApp 
      ps `shouldBe` (person2 : persons)
      logs `shouldBe` 
        [ "Applications Test testApp getPerson 2"
        , "Interpreters DbSql getPersonFromId 2"
        , "Applications Test testApp getPersons"
        , "Interpreters DbSql getPersons"]

main :: IO ()
main = hspec spec

