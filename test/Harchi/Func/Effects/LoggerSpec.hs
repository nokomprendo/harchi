
{-# LANGUAGE FlexibleContexts #-}

{-# OPTIONS_GHC -Wno-orphans #-} 

module Harchi.Func.Effects.LoggerSpec where

import Control.Monad.Writer (MonadWriter, Writer, runWriter, tell)
import Test.Hspec

import Harchi.Func.Effects.Logger (LogMsg)

runApp :: 
  MonadWriter [String] m =>
  (LogMsg m -> Writer [String] a) -> (a, [String])
runApp app = runWriter (app logFunc)
  where
    logFunc str = tell [str]

app1 :: (MonadWriter [String] m) => LogMsg m -> m ()
app1 logMsg = do
  logMsg "foo"
  logMsg "bar"

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Effects Logger" $ do
    it "foo bar" $ runApp app1 `shouldBe` ((), ["foo", "bar"])

