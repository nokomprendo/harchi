
{-# LANGUAGE FlexibleContexts #-}

module Harchi.Func.Interpreters.DbSqlSpec where

import Control.Monad.Writer (MonadIO, WriterT, MonadWriter, tell, runWriterT)
import Test.Hspec

import Harchi.Func.Domain.Person (Person(..))
import Harchi.Func.Effects.Db (GetPersons, GetPersonFromId)
import Harchi.Func.Effects.Logger (LogMsg)
import Harchi.Func.Interpreters.DbSql (getPersonsSql, getPersonFromIdSql)

sqlFile :: FilePath
sqlFile = "persons.db"

person1, person2 :: Person
person1 = Person 1 "Haskell Curry" "USA"
person2 = Person 2 "Alan Turing" "GB"

persons :: [Person]
persons = [person1, person2]

runApp :: 
  (MonadWriter [String] m1, MonadIO m1) =>
  (LogMsg m1 -> GetPersons m1 -> GetPersonFromId m1 -> WriterT [String] m2 a) ->
  m2 (a, [String])
runApp app = runWriterT (app _logMsg _getPersons _getPersonFromdId)
  where
    _logMsg str = tell [str]
    _getPersons = getPersonsSql sqlFile _logMsg
    _getPersonFromdId = getPersonFromIdSql sqlFile _logMsg

app1 :: 
  (MonadIO m, MonadWriter [String] m) => 
  LogMsg m -> GetPersons m -> GetPersonFromId m -> m [Person]
app1 logMsg getPersons _getPersonFromId = do
  logMsg "app1"
  getPersons 

app2 :: 
  (MonadIO m, MonadWriter [String] m) => 
  LogMsg m -> GetPersons m -> GetPersonFromId m -> m [Person]
app2 _logMsg _getPersons getPersonFromId = 
  getPersonFromId 1

app3 :: 
  (MonadIO m, MonadWriter [String] m) => 
  LogMsg m -> GetPersons m -> GetPersonFromId m -> m [Person]
app3 _logMsg _getPersons getPersonFromId = 
  getPersonFromId 42

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Interpreters DbSql" $ do

    it "getPersons" $ do
      res <- runApp app1 
      res `shouldBe` (persons, ["app1", "Interpreters DbSql getPersons"])

    it "getPersonFromId 1" $ do
      res <- runApp app2 
      res `shouldBe` ([person1], ["Interpreters DbSql getPersonFromId 1"])

    it "getPersonFromId 42" $ do
      res <- runApp app3 
      res `shouldBe` ([], ["Interpreters DbSql getPersonFromId 42"])


