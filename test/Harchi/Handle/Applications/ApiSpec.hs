
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Harchi.Handle.Applications.ApiSpec where

import Network.Wai (Application)
import Servant.Server.Generic (genericServe)
import Test.Hspec
import Test.Hspec.Wai (with, get, shouldRespondWith)
import Test.Hspec.Wai.JSON (json)

import Harchi.Handle.Domain.Person (Person(..))
import Harchi.Handle.Effects.Logger qualified as Logger
import Harchi.Handle.Interpreters.DbSql qualified as DbSql
import Harchi.Handle.Applications.Api (serverRecord)

sqlFile :: FilePath
sqlFile = "persons.db"

persons :: [Person]
persons = 
  [ Person 1 "Haskell Curry" "USA"
  , Person 2 "Alan Turing" "GB"
  ]

newLoggerNull :: IO Logger.Handle
newLoggerNull = return $ Logger.Handle _logMsg
  where
    _logMsg _str = return ()

withHandleLoggerNull :: (Logger.Handle -> IO a) -> IO a
withHandleLoggerNull f = newLoggerNull >>= f

serverApp :: IO Application
serverApp = 
  withHandleLoggerNull $ \hLogger -> 
    DbSql.withHandle (DbSql.Config sqlFile) hLogger $ \hDb -> 
      return $ genericServe (serverRecord hLogger hDb)

spec :: Spec
spec = do

  with serverApp $

    describe "Applications Api" $ do

      it "person 2" $ do
        get "/person/2" `shouldRespondWith` 
          [json|
            [ {personId: 2, personName: "Alan Turing", personCountry: "GB"}
            ]
          |]

      it "home" $ do
        get "/" `shouldRespondWith` "\"hello\""

      it "person 42" $ do
        get "/person/42" `shouldRespondWith` "[]"

      it "person 1" $ do
        get "/person/1" `shouldRespondWith` 
          "[{\"personId\":1,\"personName\":\"Haskell Curry\",\"personCountry\":\"USA\"}]"

      it "persons" $ do
        get "/persons" `shouldRespondWith` 
          [json|
            [ {personId: 1, personName: "Haskell Curry", personCountry: "USA"}
            , {personId: 2, personName: "Alan Turing", personCountry: "GB"}
            ]
          |]

main :: IO ()
main = hspec spec

