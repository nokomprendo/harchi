
module Harchi.Handle.Applications.TestSpec where

import Control.Concurrent.STM
import Test.Hspec

import Harchi.Handle.Domain.Person (Person(..))
import Harchi.Handle.Effects.Logger qualified as Logger
import Harchi.Handle.Interpreters.DbSql qualified as DbSql
import Harchi.Handle.Applications.Test (testApp, TestApp)

sqlFile :: FilePath
sqlFile = "persons.db"

person1, person2 :: Person
person1 = Person 1 "Haskell Curry" "USA"
person2 = Person 2 "Alan Turing" "GB"

persons :: [Person]
persons = [person1, person2]

newtype ConfigLoggerTVar = ConfigLoggerTVar
  { cVar :: TVar [String]
  }

newLoggerTVar :: ConfigLoggerTVar -> IO Logger.Handle
newLoggerTVar cLogger = return $ Logger.Handle _logMsg
  where
    var = cVar cLogger
    _logMsg str = atomically $ modifyTVar' var (\strs -> strs ++ [str])

withHandleLoggerTVar :: ConfigLoggerTVar -> (Logger.Handle -> IO a) -> IO a
withHandleLoggerTVar cLogger f = newLoggerTVar cLogger >>= f

runTestApp :: TestApp [Person] -> ConfigLoggerTVar -> DbSql.Config -> IO [Person]
runTestApp app cLogger cDb = 
  withHandleLoggerTVar cLogger $ \hLogger -> 
    DbSql.withHandle cDb hLogger $ \hDb -> 
      app hLogger hDb 

spec :: Spec
spec = do

  describe "Applications Test" $ do

    it "logs + db" $ do
      var <- newTVarIO []
      ps <- runTestApp testApp (ConfigLoggerTVar var) (DbSql.Config sqlFile)
      ps `shouldBe` (person2 : persons)
      logs <- readTVarIO var
      logs `shouldBe` 
        [ "Applications Test testApp getPerson 2"
        , "Interpreters DbSql getPersonFromId 2"
        , "Applications Test testApp getPersons"
        , "Interpreters DbSql getPersons" ]

main :: IO ()
main = hspec spec

