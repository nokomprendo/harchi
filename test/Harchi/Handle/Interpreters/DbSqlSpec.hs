
module Harchi.Handle.Interpreters.DbSqlSpec where

import Control.Concurrent.STM
import Test.Hspec

import Harchi.Handle.Domain.Person (Person(..))
import Harchi.Handle.Effects.Db qualified as Db
import Harchi.Handle.Effects.Logger qualified as Logger
import Harchi.Handle.Interpreters.DbSql qualified as DbSql

sqlFile :: FilePath
sqlFile = "persons.db"

person1, person2 :: Person
person1 = Person 1 "Haskell Curry" "USA"
person2 = Person 2 "Alan Turing" "GB"

persons :: [Person]
persons = [person1, person2]

newtype ConfigLoggerTVar = ConfigLoggerTVar
  { cVar :: TVar [String]
  }

newLoggerTVar :: ConfigLoggerTVar -> IO Logger.Handle
newLoggerTVar cLogger = return $ Logger.Handle _logMsg
  where
    var = cVar cLogger
    _logMsg str = atomically $ modifyTVar' var (\strs -> strs ++ [str])

withHandleLoggerTVar :: ConfigLoggerTVar -> (Logger.Handle -> IO a) -> IO a
withHandleLoggerTVar cLogger f = newLoggerTVar cLogger >>= f

runApp :: (Db.Handle -> IO a) -> ConfigLoggerTVar -> DbSql.Config -> IO a
runApp app cLogger cDb = 
  withHandleLoggerTVar cLogger $ \hLogger -> 
    DbSql.withHandle cDb hLogger $ \hDb -> 
      app hDb 

app1 :: Db.Handle -> IO [Person]
app1 db = Db.getPersons db

app2 :: Db.Handle -> IO [Person]
app2 db = Db.getPersonFromId db 1

app3 :: Db.Handle -> IO [Person]
app3 db = Db.getPersonFromId db 42

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Interpreters DbSql" $ do

    it "getPersons" $ do
      var1 <- newTVarIO []
      res1 <- runApp app1 (ConfigLoggerTVar var1) (DbSql.Config sqlFile)
      res1 `shouldBe` persons
      logs1 <- readTVarIO var1
      logs1 `shouldBe` ["Interpreters DbSql getPersons"]

    it "getPersonFromId 1" $ do
      var2 <- newTVarIO []
      res2 <- runApp app2 (ConfigLoggerTVar var2) (DbSql.Config sqlFile)
      res2 `shouldBe` [person1]
      logs2 <- readTVarIO var2
      logs2 `shouldBe` ["Interpreters DbSql getPersonFromId 1"]

    it "getPersonFromId 42" $ do
      var3 <- newTVarIO []
      res3 <- runApp app3 (ConfigLoggerTVar var3) (DbSql.Config sqlFile)
      res3 `shouldBe` []
      logs3 <- readTVarIO var3
      logs3 `shouldBe` ["Interpreters DbSql getPersonFromId 42"]

