
module Harchi.Handle.Effects.LoggerSpec where

import Control.Concurrent.STM
import Test.Hspec

import Harchi.Handle.Effects.Logger qualified as Logger

newtype ConfigLoggerTVar = ConfigLoggerTVar
  { cVar :: TVar [String]
  }

newLoggerTVar :: ConfigLoggerTVar -> IO Logger.Handle
newLoggerTVar cLogger = return $ Logger.Handle _logMsg
  where
    var = cVar cLogger
    _logMsg str = atomically $ modifyTVar' var (\strs -> strs ++ [str])

withHandle :: ConfigLoggerTVar -> (Logger.Handle -> IO a) -> IO a
withHandle cLogger f = newLoggerTVar cLogger >>= f

runApp :: (Logger.Handle -> IO a) -> ConfigLoggerTVar -> IO a
runApp app cLogger = 
  withHandle cLogger $ \hLogger -> 
    app hLogger 

app1 :: Logger.Handle -> IO ()
app1 logger = do
  Logger.logMsg logger "foo"
  Logger.logMsg logger "bar"

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Effects Logger" $ do
    it "foo bar" $ do
      var1 <- newTVarIO []
      runApp app1 (ConfigLoggerTVar var1)
      logs1 <- readTVarIO var1
      logs1 `shouldBe` ["foo", "bar"]

