
module Harchi.Handle.Effects.DbSpec where

import Test.Hspec

import Harchi.Handle.Domain.Person (Person(..))
import Harchi.Handle.Effects.Db qualified as Db

person1, person2 :: Person
person1 = Person 1 "Ada Lovelace" "GB"
person2 = Person 2 "Magaret Hamilton" "USA"

persons :: [Person]
persons = [person1, person2]

newtype ConfigDbMem = ConfigDbMem
  { cPersons :: [Person]
  }

new :: ConfigDbMem -> IO Db.Handle
new cDb = 
  return $ Db.Handle _getPersons _getPersonFromId
    where
      ps = cPersons cDb
      _getPersons = return ps
      _getPersonFromId i = return $ filter ((==i) . personId) ps

withHandle :: ConfigDbMem -> (Db.Handle -> IO a) -> IO a
withHandle cDb f = new cDb >>= f

runApp :: (Db.Handle -> IO a) -> ConfigDbMem -> IO a
runApp app cDb = 
  withHandle cDb $ \hDb -> 
    app hDb 

app1 :: Db.Handle -> IO [Person]
app1 db = Db.getPersons db

app2 :: Db.Handle -> IO [Person]
app2 db = Db.getPersonFromId db 1

app3 :: Db.Handle -> IO [Person]
app3 db = Db.getPersonFromId db 42

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Effects Db" $ do
    it "getPersons" $ do
      res1 <- runApp app1 (ConfigDbMem persons)
      res1 `shouldBe` persons

    it "getPersonFromId 1" $ do
      res2 <- runApp app2 (ConfigDbMem persons)
      res2 `shouldBe` [person1]

    it "getPersonFromId 42" $ do
      res3 <- runApp app3 (ConfigDbMem persons)
      res3 `shouldBe` []

