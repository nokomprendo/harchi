
{-# LANGUAGE LambdaCase #-}

module Harchi.Free.Effects.DbSpec where

import Control.Monad.Free (foldFree)
import Control.Monad.Reader (Reader, runReader, ask, asks)
import Test.Hspec

import Harchi.Free.Domain.Person (Person(..))
import Harchi.Free.Effects.Db (DbF(..), Db, getPersons, getPersonFromId)

person1, person2 :: Person
person1 = Person 1 "Ada Lovelace" "GB"
person2 = Person 2 "Magaret Hamilton" "USA"

persons :: [Person]
persons = [person1, person2]

interpretDbReader :: DbF a -> Reader [Person] a
interpretDbReader = \case
  (GetPersons next) -> 
    asks next
  (GetPersonFromId i next) -> do
    ps <- ask
    pure . next $ filter ((==i) . personId) ps

runApp :: Db a -> a
runApp app = runReader (foldFree interpretDbReader app) persons

app1 :: Db [Person]
app1 = getPersons

app2 :: Db [Person]
app2 = getPersonFromId 1

app3 :: Db [Person]
app3 = getPersonFromId 42

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Effects Db" $ do
    it "getPersons" $ runApp app1 `shouldBe` persons
    it "getPersonFromId 1" $ runApp app2 `shouldBe` [person1]
    it "getPersonFromId 42" $ runApp app3 `shouldBe` []

