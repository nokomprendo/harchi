
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Harchi.Free.Applications.ApiSpec where

import Control.Monad.Free (foldFree)
import Network.Wai (Application)
import Servant (Handler(..))
import Servant.Server.Generic (genericServeT)
import Test.Hspec
import Test.Hspec.Wai (with, get, shouldRespondWith)
import Test.Hspec.Wai.JSON (json)

import Harchi.Free.Domain.Person (Person(..))
import Harchi.Free.Effects.Logger (LoggerF(..))
import Harchi.Free.Interpreters.DbSql (interpretDbSql)
import Harchi.Free.Applications.Api (serverRecord)
import Harchi.Free.Applications.DbLogger (DbLogger, interpretSum)

sqlFile :: FilePath
sqlFile = "persons.db"

persons :: [Person]
persons = 
  [ Person 1 "Haskell Curry" "USA"
  , Person 2 "Alan Turing" "GB"
  ]

interpretLoggerNull :: Monad m => LoggerF a -> m a
interpretLoggerNull (LogMsg _str next) = pure $ next ()

serverApp :: IO Application 
serverApp = 
  return $ genericServeT runApp serverRecord
    where
      runApp :: DbLogger Handler a -> Handler a 
      runApp = foldFree (interpretSum (interpretDbSql sqlFile) (interpretSum interpretLoggerNull id))

spec :: Spec
spec = do

  with serverApp $

    describe "Applications Api" $ do

      it "person 2" $ do
        get "/person/2" `shouldRespondWith` 
          [json|
            [ {personId: 2, personName: "Alan Turing", personCountry: "GB"}
            ]
          |]

      it "home" $ do
        get "/" `shouldRespondWith` "\"hello\""

      it "person 42" $ do
        get "/person/42" `shouldRespondWith` "[]"

      it "person 1" $ do
        get "/person/1" `shouldRespondWith` 
          "[{\"personId\":1,\"personName\":\"Haskell Curry\",\"personCountry\":\"USA\"}]"

      it "persons" $ do
        get "/persons" `shouldRespondWith` 
          [json|
            [ {personId: 1, personName: "Haskell Curry", personCountry: "USA"}
            , {personId: 2, personName: "Alan Turing", personCountry: "GB"}
            ]
          |]

main :: IO ()
main = hspec spec

