
{-# LANGUAGE FlexibleContexts #-}

module Harchi.Free.Applications.TestSpec where

import Control.Monad.Free (foldFree)
import Control.Monad.Writer (WriterT, runWriterT, tell)
import Test.Hspec

import Harchi.Free.Domain.Person (Person(..))
import Harchi.Free.Effects.Logger (LoggerF(..))
import Harchi.Free.Interpreters.DbSql (interpretDbSql)
import Harchi.Free.Applications.DbLogger (DbLogger, interpretSum)
import Harchi.Free.Applications.Test (testApp)

sqlFile :: FilePath
sqlFile = "persons.db"

person1, person2 :: Person
person1 = Person 1 "Haskell Curry" "USA"
person2 = Person 2 "Alan Turing" "GB"

persons :: [Person]
persons = [person1, person2]

interpretLoggerWriter :: LoggerF a -> WriterT [String] IO a
interpretLoggerWriter (LogMsg str next) = do
  tell [str]
  pure $ next ()

runTestApp :: DbLogger (WriterT [String] IO) [Person] -> IO ([Person], [String])
runTestApp = runWriterT . foldFree (interpretSum db (interpretSum logger id))
  where
    db = interpretDbSql "persons.db"
    logger = interpretLoggerWriter

spec :: Spec
spec = do

  describe "Applications Test" $ do

    it "logs + db" $ do
      (ps, logs) <- runTestApp testApp 
      ps `shouldBe` (person2 : persons)
      logs `shouldBe` 
        [ "Applications Test testApp getPerson 2"
        , "Applications Test testApp getPersons" ]

main :: IO ()
main = hspec spec

