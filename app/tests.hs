
import Harchi.ADT.Applications.Test as ADT
import Harchi.Baseline.Test as Baseline
import Harchi.DTALC.Applications.Test as DTALC
import Harchi.Eff.Applications.Test as Eff
import Harchi.Free.Applications.Test as Free
import Harchi.FreeT.Applications.Test as FreeT
import Harchi.Func.Applications.Test as Func
import Harchi.Handle.Applications.Test as Handle
import Harchi.HFM.Applications.Test as HFM
import Harchi.MTL.Applications.Test as MTL
import Harchi.PRF.Applications.Test as PRF
import Harchi.ReaderT.Applications.Test as ReaderT
import Harchi.ReaderT1.Applications.Test as ReaderT1
import Harchi.RIO.Applications.Test as RIO
import Harchi.TLHC.Layer1.Test as TLHC

main :: IO ()
main = do

  putStrLn "\n**** ADT ****"
  ADT.runApp ADT.testApp >>= print

  putStrLn "\n**** Baseline ****"
  Baseline.runApp Baseline.testApp >>= print

  putStrLn "\n**** DTALC ****"
  DTALC.runApp DTALC.testApp >>= print

  putStrLn "\n**** Eff ****"
  Eff.runApp Eff.testApp >>= print

  putStrLn "\n**** Free ****"
  Free.runApp Free.testApp >>= print

  putStrLn "\n**** FreeT ****"
  FreeT.runApp FreeT.testApp >>= print

  putStrLn "\n**** Func ****"
  Func.runApp Func.testApp >>= print

  putStrLn "\n**** Handle ****"
  Handle.runApp Handle.testApp >>= print

  putStrLn "\n**** HFM ****"
  HFM.runApp HFM.testApp >>= print

  putStrLn "\n**** MTL ****"
  MTL.runApp MTL.testApp >>= print

  putStrLn "\n**** PRF ****"
  PRF.runApp PRF.testApp >>= print

  putStrLn "\n**** ReaderT ****"
  ReaderT.runApp ReaderT.testApp >>= print

  putStrLn "\n**** ReaderT1 ****"
  ReaderT1.runApp ReaderT1.testApp >>= print

  putStrLn "\n**** RIO ****"
  RIO.runApp RIO.testApp >>= print

  putStrLn "\n**** TLHC ****"
  TLHC.runApp TLHC.testApp >>= print

