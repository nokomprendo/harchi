
{-# LANGUAGE OverloadedStrings #-}

import Harchi.RIO.Applications.Server (runServer)

main :: IO ()
main = runServer 3000 "harchi-rio" "persons.db"

