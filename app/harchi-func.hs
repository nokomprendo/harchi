
import Harchi.Func.Applications.Server (runServer)

main :: IO ()
main = runServer 3000 "harchi-func" "persons.db"

