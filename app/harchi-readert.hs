
import Harchi.ReaderT.Applications.Server (runServer)

main :: IO ()
main = runServer 3000 "harchi-readert" "persons.db"

