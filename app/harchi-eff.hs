
import Harchi.Eff.Applications.Server (runServer)

main :: IO ()
main = runServer 3000 "harchi-eff" "persons.db"

