
import Harchi.ADT.Applications.Server (runServer)

main :: IO ()
main = runServer 3000 "harchi-adt" "persons.db"

