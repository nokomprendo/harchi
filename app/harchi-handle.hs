
import Harchi.Handle.Applications.Server (runServer)

main :: IO ()
main = runServer 3000 "harchi-handle" "persons.db"

