
let
  # rev = "515e192f40281ba09843ff17b9c926ff77c67487";
  # url = "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
  url = "https://github.com/NixOS/nixpkgs/archive/refs/tags/23.11.tar.gz";
  pkgs = import (fetchTarball url) {};
in pkgs.callPackage ./default.nix {}

