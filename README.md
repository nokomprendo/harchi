
## Run

```sh
nix-shell

sqlite3 persons.db < persons.sql

cabal build
cabal run harchi-clean
...

cabal test --test-show-details=always
```

## Planning

- [x] Baseline
- [x] Func

- [x] ADT
- [x] Handle

- [x] Eff

- [x] MTL 

- [x] Free
- [x] Hfm
- [x] FreeT

- [x] ReaderT 
- [x] Three Layer Haskell Cake

- [x] DTALC


## References

### Injection de dépendances

- https://en.wikipedia.org/wiki/Dependency_injection

### Handle/Service pattern

- https://www.schoolofhaskell.com/user/meiersi/the-service-pattern
- https://jaspervdj.be/posts/2018-03-08-handle-pattern.html
- https://brandonchinn178.github.io/posts/2023/05/03/services-design-pattern
- https://github.com/jaspervdj/fugacious

### MTL-style, transformateurs de monades

- https://serokell.io/blog/introduction-tagless-final
- https://github.com/lexi-lambda/mtl-style-example/blob/master/library/MTLStyleExample/Interfaces.hs

### ReaderT pattern

- https://www.fpcomplete.com/blog/readert-design-pattern/
- https://magnus.therning.org/2019-02-02-000-the-readert-design-pattern-or-tagless-final-.html
- https://nicolashery.com/nesting-reader-environments-servant/
- https://docs.servant.dev/en/stable/cookbook/generic/Generic.html
- https://gist.github.com/nicolashery/7c3b3b2a57443a1fa336a4dfa498913d

### Free monads

- https://hackage.haskell.org/package/free-5.2/docs/Control-Monad-Free.html
- https://hackage.haskell.org/package/base/docs/Data-Functor-Sum.html
- https://blog.ploeh.dk/2017/07/24/combining-free-monads-in-haskell/
- https://markkarpov.com/post/free-monad-considered-harmful.html

### Systèmes d'effets

- https://thma.github.io/posts/2022-07-04-polysemy-and-warp.html
- https://github.com/thma/PolysemyCleanArchitecture
- https://github.com/AJChapman/servant-polysemy/blob/master/example/ServerGeneric.hs

### Transformateurs de free monads

- https://blog.ploeh.dk/2017/07/24/combining-free-monads-in-haskell/
- https://hackage.haskell.org/package/free-5.2/docs/Control-Monad-Trans-Free.html

### Hierarchical Free Monads

- https://github.com/graninas/hierarchical-free-monads-the-most-developed-approach-in-haskell/blob/master/README.md
- https://github.com/graninas/software-design-in-haskell

### Three Layer Haskell Cake

- https://www.parsonsmatt.org/2018/03/22/three_layer_haskell_cake.html
- https://www.parsonsmatt.org/2017/07/27/inverted_mocking.html

### Clean architecture 

- https://github.com/thma/PolysemyCleanArchitecture
- https://thma.github.io/posts/2022-07-04-polysemy-and-warp.html
- https://thma.github.io/posts/2020-05-29-polysemy-clean-architecture.html
- https://www.youtube.com/watch?v=kIwd1D9m1gE

### Datatypes a la carte

- http://lambda-the-ultimate.org/node/2700
- https://hackage.haskell.org/package/compdata
- https://reasonablypolymorphic.com/blog/better-data-types-a-la-carte/index.html

### Polymorphic Record of Functions

- https://gvolpe.com/blog/lessons-learned-while-writing-a-haskell-app/
- https://github.com/gvolpe/exchange-rates

