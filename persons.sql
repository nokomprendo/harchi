-- sqlite3 persons.db < persons.sql

CREATE TABLE persons (
    personId INTEGER PRIMARY KEY AUTOINCREMENT,
    personName TEXT,
    personCountry TEXT
);

INSERT INTO persons(personName, personCountry) VALUES('Haskell Curry', 'USA');
INSERT INTO persons(personName, personCountry) VALUES('Alan Turing', 'GB');

