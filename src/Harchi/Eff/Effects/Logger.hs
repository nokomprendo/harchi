
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE TemplateHaskell #-}

module Harchi.Eff.Effects.Logger where

import Polysemy (makeSem)

data Logger m a where
  LogMsg :: String -> Logger m ()

makeSem ''Logger

