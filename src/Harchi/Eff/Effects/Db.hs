
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE TemplateHaskell #-}

module Harchi.Eff.Effects.Db where

import Polysemy (makeSem)

import Harchi.Eff.Domain.Person (Person)

data Db m a where
  GetPersons :: Db m [Person]
  GetPersonFromId :: Int -> Db m [Person]

makeSem ''Db

