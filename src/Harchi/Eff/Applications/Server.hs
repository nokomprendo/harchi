
{-# LANGUAGE DataKinds #-}

module Harchi.Eff.Applications.Server where

import Control.Monad.Trans.Except (ExceptT(..))
import Network.Wai.Handler.Warp (run)
import Polysemy hiding (run)
import Servant (Handler(..))
import Servant.Server.Generic (genericServeT)

import Harchi.Eff.Effects.Db (Db)
import Harchi.Eff.Effects.Logger (Logger)
import Harchi.Eff.Interpreters.DbSql (runDbSql)
import Harchi.Eff.Interpreters.LoggerStdout (runLoggerStdout)
import Harchi.Eff.Applications.Api (serverRecord)

liftToHandler :: IO a -> Handler a
liftToHandler = Handler . ExceptT . fmap Right

runServer :: Int -> String -> FilePath -> IO ()
runServer port prefix sqlFile = do

  putStrLn $ "listening on port " ++ show port ++ "..." 

  let runApp :: Sem '[Db, Logger, Embed IO] a -> Handler a
      runApp = liftToHandler . runM . runLoggerStdout prefix . runDbSql sqlFile

  run port $ genericServeT runApp serverRecord

