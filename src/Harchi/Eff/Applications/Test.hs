
{-# LANGUAGE DataKinds #-}

module Harchi.Eff.Applications.Test where

import Polysemy 

import Harchi.Eff.Domain.Person (Person)
import Harchi.Eff.Effects.Db (Db, getPersons, getPersonFromId)
import Harchi.Eff.Effects.Logger (Logger, logMsg)
import Harchi.Eff.Interpreters.DbSql (runDbSql)
import Harchi.Eff.Interpreters.LoggerStdout (runLoggerStdout)

testApp :: Members '[Db, Logger] r => Sem r [Person]
testApp = do
  logMsg "Applications Test testApp getPerson 2"
  res1 <- getPersonFromId 2 
  logMsg "Applications Test testApp getPersons"
  res2 <- getPersons 
  return (res1 ++ res2)

runApp :: Sem '[Db, Logger, Embed IO] a -> IO a
runApp app = runM $ logger $ db app
  where
    logger = runLoggerStdout "Eff" 
    db = runDbSql "persons.db" 

