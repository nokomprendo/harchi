
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Harchi.Eff.Applications.Api where

import Data.Text.Lazy (Text)
import GHC.Generics (Generic)
import Polysemy hiding (run)
import Servant ((:>), (:-), Get, Capture, JSON)
import Servant.Server.Generic (AsServerT)

import Harchi.Eff.Domain.Person (Person(..))
import Harchi.Eff.Effects.Db (Db, getPersons, getPersonFromId)
import Harchi.Eff.Effects.Logger (Logger, logMsg)

data Routes route = Routes
  { _home :: route :- Get '[JSON] Text
  , _persons :: route :- "persons" :> Get '[JSON] [Person]
  , _person :: route :- "person" :> Capture "i" Int :> Get '[JSON] [Person]
  } deriving (Generic)

serverRecord :: Members '[Db, Logger] r => Routes (AsServerT (Sem r))
serverRecord = Routes
  { _home = do
      logMsg "Applications Api serverRecord /"
      pure "hello"
  , _persons = do
      logMsg "Applications Api serverRecord /persons"
      getPersons
  , _person = \i -> do
      logMsg $ "Applications Api serverRecord /person/" <> show i 
      getPersonFromId i
  }

