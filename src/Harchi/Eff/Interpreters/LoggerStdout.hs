
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}

module Harchi.Eff.Interpreters.LoggerStdout where

import Control.Monad.IO.Class (liftIO)
import Polysemy (Embed, Member, Sem, interpret)

import Harchi.Eff.Effects.Logger (Logger(..))

runLoggerStdout :: 
  Member (Embed IO) r => 
  String -> Sem (Logger ': r) a -> Sem r a
runLoggerStdout prefix = interpret \case
  LogMsg str -> liftIO $ putStrLn $ "[" <> prefix <> "] " <> str

