
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

{-# OPTIONS_GHC -Wno-orphans #-} 

module Harchi.Eff.Interpreters.DbSql where

import Control.Monad.IO.Class (liftIO)
import Database.SQLite.Simple (FromRow, field, fromRow, withConnection, query, query_, Only(..))
import Polysemy (Embed, Members, Sem, interpret)

import Harchi.Eff.Domain.Person (Person(..))
import Harchi.Eff.Effects.Db (Db(..))
import Harchi.Eff.Effects.Logger (Logger, logMsg)

instance FromRow Person where
  fromRow = Person <$> field <*> field <*> field 

runDbSql :: 
  Members '[Logger, Embed IO] r => 
  FilePath -> Sem (Db ': r) a -> Sem r a
runDbSql sqlFile = interpret \case

  GetPersons -> do
    logMsg "Interpreters DbSql runDbSql GetPersons"
    liftIO $ withConnection sqlFile $ \conn -> 
      query_ conn 
        "SELECT personId, personName, personCountry \
        \FROM persons"

  GetPersonFromId i -> do
    logMsg $ "Interpreters DbSql runDbSql GetPersonFromId " <> show i
    liftIO $ withConnection sqlFile $ \conn -> 
      query conn 
        "SELECT personId, personName, personCountry \
        \FROM persons \
        \WHERE personId = (?)" (Only i)

