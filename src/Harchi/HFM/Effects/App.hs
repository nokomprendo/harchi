
{-# LANGUAGE GADTs #-}

module Harchi.HFM.Effects.App where

import Control.Monad.Free (Free, liftF)

import Harchi.HFM.Effects.Db (Db)
import Harchi.HFM.Effects.Logger (Logger)

data AppF a where
  AppLogger :: Logger r -> (r -> a) -> AppF a
  AppDb :: Db r -> (r -> a) -> AppF a
  -- AppIO :: IO r -> (r -> a) -> AppF a

instance Functor AppF where
  fmap f (AppLogger action next) = AppLogger action (f . next)
  fmap f (AppDb action next) = AppDb action (f . next)
  -- fmap f (AppIO action next) = AppIO action (f . next)

type App = Free AppF

appLogger :: Logger a -> App a
appLogger action = liftF $ AppLogger action id

appDb :: Db a -> App a
appDb action = liftF $ AppDb action id

-- appIO :: IO a -> App a
-- appIO action = liftF $ AppIO action id

