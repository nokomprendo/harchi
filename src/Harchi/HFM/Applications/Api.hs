
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Harchi.HFM.Applications.Api where

import Data.Text.Lazy (Text)
import GHC.Generics (Generic)
import Servant ((:>), (:-), Get, Capture, JSON)
import Servant.Server.Generic (AsServerT)

import Harchi.HFM.Domain.Person (Person(..))
import Harchi.HFM.Effects.App (App, appLogger, appDb)
import Harchi.HFM.Effects.Db (getPersons, getPersonFromId)
import Harchi.HFM.Effects.Logger (logMsg)

data Routes route = Routes
  { _home :: route :- Get '[JSON] Text
  , _persons :: route :- "persons" :> Get '[JSON] [Person]
  , _person :: route :- "person" :> Capture "i" Int :> Get '[JSON] [Person]
  } deriving (Generic)

serverRecord :: Routes (AsServerT App)
serverRecord = Routes
  { _home = do
      appLogger $ logMsg "Applications Api serverRecord /"
      pure "hello"
  , _persons = do
      appLogger $ logMsg "Applications Api serverRecord /persons"
      appDb getPersons
  , _person = \i -> do
      appLogger $ logMsg $ "Applications Api serverRecord /person/" <> show i 
      appDb (getPersonFromId i)
  }

