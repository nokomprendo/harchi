
module Harchi.HFM.Applications.Server where

import Network.Wai.Handler.Warp (run)
import Servant (Handler(..))
import Servant.Server.Generic (genericServeT)

import Harchi.HFM.Effects.App (App)
import Harchi.HFM.Interpreters.AppSql (runAppSql)
import Harchi.HFM.Applications.Api (serverRecord)

runServer :: Int -> String -> FilePath -> IO ()
runServer port prefix sqlFile = do

  putStrLn $ "listening on port " ++ show port ++ "..." 

  let runApp :: App a -> Handler a 
      runApp = runAppSql prefix sqlFile

  run port $ genericServeT runApp serverRecord

