
module Harchi.HFM.Applications.Test where

import Harchi.HFM.Domain.Person (Person)
import Harchi.HFM.Effects.App (App, appLogger, appDb)
import Harchi.HFM.Effects.Db (getPersons, getPersonFromId)
import Harchi.HFM.Effects.Logger (logMsg)
import Harchi.HFM.Interpreters.AppSql (runAppSql)

testApp :: App [Person]
testApp = do
  appLogger $ logMsg "Applications Test testApp getPerson 2"
  res1 <- appDb (getPersonFromId 2) 
  appLogger $ logMsg "Applications Test testApp getPersons"
  res2 <- appDb getPersons 
  return (res1 ++ res2)

runApp :: App a -> IO a
runApp app = runAppSql "HFM" "persons.db" app

