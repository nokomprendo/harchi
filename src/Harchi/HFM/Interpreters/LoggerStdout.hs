
module Harchi.HFM.Interpreters.LoggerStdout where

import Control.Monad.Free (foldFree)
import Control.Monad.IO.Class (MonadIO, liftIO)

import Harchi.HFM.Effects.Logger (LoggerF(..), Logger)

interpretLoggerStdout :: MonadIO m => String -> LoggerF a -> m a
interpretLoggerStdout prefix (LogMsg str next) = do
  liftIO $ putStrLn $ "[" <> prefix <> "] " <> str
  pure $ next ()

runLoggerStdout :: MonadIO m => String -> Logger a -> m a
runLoggerStdout prefix = foldFree (interpretLoggerStdout prefix)

