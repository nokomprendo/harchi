
{-# LANGUAGE LambdaCase #-}

module Harchi.HFM.Interpreters.AppSql where

import Control.Monad.Free (foldFree)
import Control.Monad.IO.Class (MonadIO)
-- import Control.Monad.IO.Class (MonadIO, liftIO)

import Harchi.HFM.Effects.App (App, AppF(..))
import Harchi.HFM.Interpreters.DbSql (runDbSql)
import Harchi.HFM.Interpreters.LoggerStdout (runLoggerStdout)

interpretAppSql :: MonadIO m => String -> FilePath -> AppF a -> m a
interpretAppSql prefix sqlFile = \case
  (AppLogger action next) -> next <$> runLoggerStdout prefix action 
  (AppDb action next)     -> next <$> runDbSql sqlFile action 
  -- (AppIO action next)     -> next <$> liftIO action

runAppSql :: MonadIO m => String -> FilePath -> App a -> m a
runAppSql prefix sqlFile = foldFree (interpretAppSql prefix sqlFile)

