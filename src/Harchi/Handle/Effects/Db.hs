
module Harchi.Handle.Effects.Db where

import Harchi.Handle.Domain.Person (Person)

data Handle = Handle
  { getPersons :: IO [Person]
  , getPersonFromId :: Int -> IO [Person]
  }

