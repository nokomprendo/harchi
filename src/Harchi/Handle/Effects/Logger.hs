
module Harchi.Handle.Effects.Logger where

newtype Handle = Handle
  { logMsg :: String -> IO ()
  }

