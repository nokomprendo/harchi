
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Harchi.Handle.Applications.Api where

import Control.Monad.IO.Class (liftIO)
import Data.Text (Text)
import GHC.Generics (Generic)
import Servant ((:>), (:-), Get, Capture, JSON)
import Servant.Server.Generic (AsServer)

import Harchi.Handle.Domain.Person (Person)
import Harchi.Handle.Effects.Db qualified as Db
import Harchi.Handle.Effects.Logger qualified as Logger

data Routes route = Routes
  { _home :: route :- Get '[JSON] Text
  , _persons :: route :- "persons" :> Get '[JSON] [Person]
  , _person :: route :- "person" :> Capture "i" Int :> Get '[JSON] [Person]
  } deriving (Generic)

serverRecord :: Logger.Handle -> Db.Handle -> Routes AsServer
serverRecord hLogger hDb = Routes
  { _home = liftIO $ do
      Logger.logMsg hLogger "Applications Api serverRecord /"
      pure "hello"
  , _persons = liftIO $ do
      Logger.logMsg hLogger "Applications Api serverRecord /persons"
      Db.getPersons hDb
  , _person = \i -> liftIO $ do
      Logger.logMsg hLogger $ "Applications Api serverRecord /person/" <> show i 
      Db.getPersonFromId hDb i
  }

