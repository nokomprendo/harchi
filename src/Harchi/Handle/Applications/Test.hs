
module Harchi.Handle.Applications.Test where

import Harchi.Handle.Domain.Person (Person)
import Harchi.Handle.Effects.Logger qualified as Logger
import Harchi.Handle.Effects.Db qualified as Db
import Harchi.Handle.Interpreters.LoggerStdout qualified as LoggerStdout
import Harchi.Handle.Interpreters.DbSql qualified as DbSql

type TestApp a = Logger.Handle -> Db.Handle -> IO a

testApp :: TestApp [Person]
testApp hLogger hDb = do
  Logger.logMsg hLogger "Applications Test testApp getPerson 2"
  res1 <- Db.getPersonFromId hDb 2 
  Logger.logMsg hLogger "Applications Test testApp getPersons"
  res2 <- Db.getPersons hDb 
  return (res1 ++ res2)

runApp :: TestApp a -> IO a
runApp app = 
  LoggerStdout.withHandle cLogger $ \hLogger -> 
    DbSql.withHandle cDb hLogger $ \hDb ->
      app hLogger hDb
  where
    cLogger = LoggerStdout.Config "Handle"
    cDb = DbSql.Config "persons.db"

