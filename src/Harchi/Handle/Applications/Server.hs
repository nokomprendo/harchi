
module Harchi.Handle.Applications.Server where

import Network.Wai.Handler.Warp (run)
import Servant.Server.Generic (genericServe)

import Harchi.Handle.Interpreters.DbSql qualified as DbSql
import Harchi.Handle.Interpreters.LoggerStdout qualified as LoggerStdout
import Harchi.Handle.Applications.Api (serverRecord)

runServer :: Int -> String -> FilePath -> IO ()
runServer port prefix sqlFile = do

  putStrLn $ "listening on port " ++ show port ++ "..." 

  let cLogger = LoggerStdout.Config prefix
      cDb = DbSql.Config sqlFile

  LoggerStdout.withHandle cLogger $ \hLogger -> 
    DbSql.withHandle cDb hLogger $ \hDb ->
      run port $ genericServe (serverRecord hLogger hDb)

