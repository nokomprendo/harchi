
{-# LANGUAGE OverloadedStrings #-}

{-# OPTIONS_GHC -Wno-orphans #-} 

module Harchi.Handle.Interpreters.DbSql where

import Database.SQLite.Simple (FromRow, field, withConnection, Only(..), fromRow, query, query_)

import Harchi.Handle.Domain.Person (Person(..))
import Harchi.Handle.Effects.Db qualified as Db
import Harchi.Handle.Effects.Logger qualified as Logger

instance FromRow Person where
    fromRow = Person <$> field <*> field <*> field

newtype Config = Config
  { cSqlFile :: FilePath
  }

new :: Config -> Logger.Handle -> IO Db.Handle
new config hLogger =
  return $ Db.Handle  getPersonsSql getPersonFromIdSql

    where
      sqlFile = cSqlFile config

      getPersonsSql = withConnection sqlFile $ \conn -> do
        Logger.logMsg hLogger "Interpreters DbSql getPersons"
        query_ conn 
          "SELECT personId, personName, personCountry \
          \FROM persons"

      getPersonFromIdSql i = withConnection sqlFile $ \conn -> do
        Logger.logMsg hLogger $ "Interpreters DbSql getPersonFromId " <> show i 
        query conn 
          "SELECT personId, personName, personCountry \
          \FROM persons \
          \WHERE personId = (?)" (Only i)

withHandle :: Config -> Logger.Handle -> (Db.Handle -> IO a) -> IO a
withHandle config logger f = new config logger >>= f

