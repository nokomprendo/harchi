
module Harchi.Handle.Interpreters.LoggerStdout where

import Harchi.Handle.Effects.Logger as Logger (Handle(..))

newtype Config = Config
  { cPrefix :: String
  }

new :: Config -> IO Handle
new config = return $ Handle logMsgStdout
  where
    prefix  = cPrefix config
    logMsgStdout str = putStrLn $ "[" <> prefix <> "] " <> str

withHandle :: Config -> (Handle -> IO a) -> IO a
withHandle config f = new config >>= f

