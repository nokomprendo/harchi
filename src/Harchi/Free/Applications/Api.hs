
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Harchi.Free.Applications.Api where

import Data.Text.Lazy (Text)
import GHC.Generics (Generic)
import Servant ((:>), (:-), Get, Capture, JSON, Handler)
import Servant.Server.Generic (AsServerT)

import Harchi.Free.Domain.Person (Person(..))
import Harchi.Free.Effects.Db (getPersons, getPersonFromId)
import Harchi.Free.Effects.Logger (logMsg)
import Harchi.Free.Applications.DbLogger (liftLogger, liftDb, DbLogger)

data Routes route = Routes
  { _home :: route :- Get '[JSON] Text
  , _persons :: route :- "persons" :> Get '[JSON] [Person]
  , _person :: route :- "person" :> Capture "i" Int :> Get '[JSON] [Person]
  } deriving (Generic)

serverRecord :: Routes (AsServerT (DbLogger Handler))
serverRecord = Routes
  { _home = do
      liftLogger $ logMsg "Applications Api serverRecord /"
      pure "hello"
  , _persons = do
      liftLogger $ logMsg "Applications Api serverRecord /persons"
      liftDb getPersons
  , _person = \i -> do
      liftLogger $ logMsg $ "Applications Api serverRecord /person/" <> show i 
      liftDb (getPersonFromId i)
  }

