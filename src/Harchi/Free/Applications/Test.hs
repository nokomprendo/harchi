
module Harchi.Free.Applications.Test where

import Control.Monad.Free (foldFree)
import Control.Monad.IO.Class (MonadIO)

import Harchi.Free.Domain.Person (Person)
import Harchi.Free.Effects.Db (getPersons, getPersonFromId)
import Harchi.Free.Effects.Logger (logMsg)
import Harchi.Free.Interpreters.DbSql (interpretDbSql)
import Harchi.Free.Interpreters.LoggerStdout (interpretLoggerStdout)
import Harchi.Free.Applications.DbLogger (liftLogger, liftDb, DbLogger, interpretSum)

testApp :: MonadIO m => DbLogger m [Person]
testApp = do
  liftLogger $ logMsg "Applications Test testApp getPerson 2"
  res1 <- liftDb (getPersonFromId 2) 
  liftLogger $ logMsg "Applications Test testApp getPersons"
  res2 <- liftDb getPersons 
  return (res1 ++ res2)

runApp :: DbLogger IO a -> IO a
runApp app = foldFree (interpretSum db (interpretSum logger id)) app
  where
    db = interpretDbSql "persons.db"
    logger = interpretLoggerStdout "Free"

