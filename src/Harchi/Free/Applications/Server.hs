
module Harchi.Free.Applications.Server where

import Control.Monad.Free (foldFree)
import Network.Wai.Handler.Warp (run)
import Servant (Handler(..))
import Servant.Server.Generic (genericServeT)

import Harchi.Free.Interpreters.DbSql (interpretDbSql)
import Harchi.Free.Interpreters.LoggerStdout (interpretLoggerStdout)
import Harchi.Free.Applications.Api (serverRecord)
import Harchi.Free.Applications.DbLogger (DbLogger, interpretSum)

runServer :: Int -> String -> FilePath -> IO ()
runServer port prefix sqlFile = do

  putStrLn $ "listening on port " ++ show port ++ "..." 

  let 
      db = interpretDbSql sqlFile
      logger = interpretLoggerStdout prefix

      runApp :: DbLogger Handler a -> Handler a 
      runApp = foldFree (interpretSum db (interpretSum logger id))

  run port $ genericServeT runApp serverRecord

