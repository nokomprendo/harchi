
module Harchi.Free.Applications.DbLogger where

import Control.Monad.Free (Free, hoistFree)
import Data.Functor.Sum (Sum(..))

import Harchi.Free.Effects.Db (Db, DbF)
import Harchi.Free.Effects.Logger (LoggerF, Logger)

-- liftL :: (Functor f, Functor g) => Free f a -> Free (Sum f g) a
-- liftL = hoistFree InL

-- liftR :: (Functor f, Functor g) => Free g a -> Free (Sum f g) a
-- liftR = hoistFree InR

type DbLoggerF m = Sum DbF (Sum LoggerF m)

type DbLogger m = Free (DbLoggerF m)

liftDb :: Monad m => Db a -> DbLogger m a
liftDb = hoistFree InL

liftLogger :: Monad m => Logger a -> DbLogger m a
liftLogger = hoistFree InR . hoistFree InL

interpretSum :: (f a -> t a) -> (g a -> t a) -> Sum f g a -> t a
interpretSum interpretL _ (InL x) = interpretL x
interpretSum _ interpretR (InR x) = interpretR x

