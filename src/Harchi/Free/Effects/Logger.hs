
{-# LANGUAGE GADTs #-}

module Harchi.Free.Effects.Logger where

import Control.Monad.Free (Free, liftF)

-- data LoggerF a
--   = LogMsg String (() -> a)
--   deriving (Functor)

data LoggerF a where
  LogMsg :: String -> (() -> a) -> LoggerF a
  deriving Functor

type Logger = Free LoggerF

logMsg :: String -> Logger ()
logMsg msg = liftF $ LogMsg msg id

