
{-# LANGUAGE GADTs #-}

module Harchi.Free.Effects.Db where

import Control.Monad.Free (Free, liftF)

import Harchi.Free.Domain.Person (Person(..))

-- data DbF a
--   = GetPersons ([Person] -> a)
--   | GetPersonFromId Int ([Person] -> a)
--   deriving (Functor)

data DbF a where
  GetPersons :: ([Person] -> a) -> DbF a
  GetPersonFromId :: Int -> ([Person] -> a) -> DbF a
  deriving Functor

type Db = Free DbF

getPersons :: Db [Person]
getPersons = liftF $ GetPersons id

getPersonFromId :: Int -> Db [Person]
getPersonFromId i = liftF $ GetPersonFromId i id

