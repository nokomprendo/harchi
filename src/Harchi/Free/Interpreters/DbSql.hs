
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

{-# OPTIONS_GHC -Wno-orphans #-} 

module Harchi.Free.Interpreters.DbSql where

import Control.Monad.Free (foldFree)
import Control.Monad.IO.Class (liftIO, MonadIO)
import Database.SQLite.Simple (FromRow, field, withConnection, Only(..), fromRow, query, query_)

import Harchi.Free.Domain.Person (Person(..))
import Harchi.Free.Effects.Db (Db, DbF(..))

instance FromRow Person where
  fromRow = Person <$> field <*> field <*> field 

interpretDbSql :: MonadIO m => FilePath -> DbF a -> m a
interpretDbSql sqlFile = \case

  GetPersons next -> do
    fmap next $ liftIO $ withConnection sqlFile $ \conn -> 
      query_ conn 
        "SELECT personId, personName, personCountry \
        \FROM persons"

  GetPersonFromId i next -> do
    fmap next $ liftIO $ withConnection sqlFile $ \conn -> 
      query conn 
        "SELECT personId, personName, personCountry \
        \FROM persons \
        \WHERE personId = (?)" (Only i)

runDbSql :: MonadIO m => FilePath -> Db a -> m a
runDbSql sqlFile = foldFree (interpretDbSql sqlFile)

