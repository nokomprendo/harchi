
{-# LANGUAGE LambdaCase #-}

module Harchi.Free.Interpreters.LoggerStdout where

import Control.Monad.Free (foldFree)
import Control.Monad.IO.Class (MonadIO, liftIO)

import Harchi.Free.Effects.Logger (LoggerF(..), Logger, logMsg)

interpretLoggerStdout :: MonadIO m => String -> LoggerF a -> m a
interpretLoggerStdout prefix = \case
  LogMsg str next -> do
    liftIO $ putStrLn $ "[" <> prefix <> "] " <> str
    pure $ next ()

runLoggerStdout :: MonadIO m => String -> Logger a -> m a
runLoggerStdout prefix = foldFree (interpretLoggerStdout prefix)

appLogger1 :: Logger ()
appLogger1 = do
    logMsg "foo"
    logMsg "bar"

-- >>> runLoggerStdout "test" appLogger1
-- [test] foo
-- [test] bar
--
