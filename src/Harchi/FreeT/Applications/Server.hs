
module Harchi.FreeT.Applications.Server where

-- import Control.Monad.IO.Class (MonadIO)
-- import Control.Monad.Trans.Identity
import Network.Wai.Handler.Warp (run)
import Servant (Handler(..))
import Servant.Server.Generic (genericServeT)

import Harchi.FreeT.Effects.Logger -- (LoggerT)
import Harchi.FreeT.Effects.Db (DbT)
import Harchi.FreeT.Interpreters.DbSql (runDbSql)
import Harchi.FreeT.Interpreters.LoggerStdout (runLoggerStdout)
import Harchi.FreeT.Applications.Api (serverRecord)

runServer :: Int -> String -> FilePath -> IO ()
runServer port prefix sqlFile = do

  putStrLn $ "listening on port " ++ show port ++ "..." 

  let
    runApp :: DbT (LoggerT Handler) a -> Handler a 
    runApp = runLoggerStdout prefix . runDbSql sqlFile

  run port $ genericServeT runApp serverRecord


-- runServer :: Int -> String -> FilePath -> IO ()
-- runServer port prefix sqlFile = do

--   putStrLn $ "listening on port " ++ show port ++ "..." 

--   let
--     logger :: MonadIO m => LoggerT m a -> m a
--     logger = runIdentityT . runLoggerStdout prefix

--     db :: (MonadIO m, MonadLogger m) => DbT m a -> m a
--     db = logger . runDbSql sqlFile

--     runApp :: DbT (LoggerT Handler) a -> Handler a 
--     runApp = logger . db

--   run port $ genericServeT runApp serverRecord

