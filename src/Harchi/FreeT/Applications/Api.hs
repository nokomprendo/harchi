
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Harchi.FreeT.Applications.Api where

import Data.Text.Lazy (Text)
import GHC.Generics (Generic)
import Servant ((:>), (:-), Get, Capture, JSON)
import Servant.Server.Generic (AsServerT)

import Harchi.FreeT.Domain.Person (Person(..))
import Harchi.FreeT.Effects.Db (getPersons, getPersonFromId, MonadDb)
import Harchi.FreeT.Effects.Logger (logMsg, MonadLogger)

data Routes route = Routes
  { _home :: route :- Get '[JSON] Text
  , _persons :: route :- "persons" :> Get '[JSON] [Person]
  , _person :: route :- "person" :> Capture "i" Int :> Get '[JSON] [Person]
  } deriving (Generic)

serverRecord :: (MonadLogger m, MonadDb m) => Routes (AsServerT m)
serverRecord = Routes
  { _home = do
      logMsg "Applications Api serverRecord /"
      pure "hello"
  , _persons = do
      logMsg "Applications Api serverRecord /persons"
      getPersons
  , _person = \i -> do
      logMsg $ "Applications Api serverRecord /person/" <> show i 
      getPersonFromId i
  }

