
module Harchi.FreeT.Applications.Test where

-- import Control.Monad.Trans.Identity
import Control.Monad.IO.Class (MonadIO)

import Harchi.FreeT.Domain.Person (Person)
import Harchi.FreeT.Effects.Db (DbT, MonadDb, getPersons, getPersonFromId)
import Harchi.FreeT.Effects.Logger (LoggerT, MonadLogger, logMsg)
import Harchi.FreeT.Interpreters.DbSql (runDbSql)
import Harchi.FreeT.Interpreters.LoggerStdout (runLoggerStdout)

testApp :: (MonadLogger m, MonadDb m, MonadIO m) => m [Person]
testApp = do
  logMsg "Applications Test testApp getPerson 2"
  res1 <- getPersonFromId 2 
  logMsg "Applications Test testApp getPersons"
  res2 <- getPersons 
  return (res1 ++ res2)

runApp :: DbT (LoggerT IO) a -> IO a
runApp app = logger $ db app
  where    
    logger = runLoggerStdout "FreeT"
    db = runDbSql "persons.db"

-- runApp :: DbT (LoggerT IO) a -> IO a
-- runApp app = logger $ db app
--   where    
--     logger :: MonadIO m => LoggerT m a -> m a
--     logger = runIdentityT . runLoggerStdout "FreeT App"

--     db :: (MonadIO m, MonadLogger m) => DbT m a -> m a
--     db = runIdentityT . runLoggerStdout "FreeT Db" . runDbSql "persons.db"

