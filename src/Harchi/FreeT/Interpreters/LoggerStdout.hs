
{-# LANGUAGE LambdaCase #-}

module Harchi.FreeT.Interpreters.LoggerStdout where

import Control.Monad (join)
import Control.Monad.IO.Class (MonadIO, liftIO)
-- import Control.Monad.Trans.Class (lift, MonadTrans)
import Control.Monad.Trans.Free -- (FreeT(..), FreeF(..))

import Harchi.FreeT.Effects.Logger (LoggerT, LoggerF(..))

-- runLoggerStdout :: MonadIO m => String -> LoggerT m r -> m r
-- runLoggerStdout prefix app = do
--   x0 <- runFreeT app
--   case x0 of
--     Pure r -> return r
--     Free (LogMsg str next) -> do
--       liftIO $ putStrLn $ "[" <> prefix <> "] " <> str
--       runLoggerStdout prefix (next ())

interpretLoggerStdout :: MonadIO m => String -> LoggerF a -> m a
interpretLoggerStdout prefix = \case
  LogMsg str next -> do
    liftIO $ putStrLn $ "[" <> prefix <> "] " <> str
    pure $ next ()

runLoggerStdout :: MonadIO m => String -> LoggerT m a -> m a
runLoggerStdout prefix = iterT (join . interpretLoggerStdout prefix)

-- runLoggerStdout :: 
--   (MonadTrans t, Monad m, MonadIO (t m)) =>
--   String -> LoggerT m r -> t m r
-- runLoggerStdout prefix = myFoldFreeT (interpretLoggerStdout prefix)

