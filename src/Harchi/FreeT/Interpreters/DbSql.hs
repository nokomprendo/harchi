
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

{-# OPTIONS_GHC -Wno-orphans #-} 

module Harchi.FreeT.Interpreters.DbSql where

import Control.Monad
import Control.Monad.IO.Class (MonadIO, liftIO)
-- import Control.Monad.Trans.Class (lift, MonadTrans)
import Control.Monad.Trans.Free -- (FreeT(..), FreeF(..))
import Database.SQLite.Simple (FromRow, field, withConnection, Only(..), fromRow, query, query_)

import Harchi.FreeT.Domain.Person (Person(..))
import Harchi.FreeT.Effects.Db (DbT, DbF(..))
import Harchi.FreeT.Effects.Logger -- (MonadLogger, logMsg)

instance FromRow Person where
  fromRow = Person <$> field <*> field <*> field 

-- runDbSql :: (MonadLogger m, MonadIO m) => FilePath -> DbT m r -> m r
-- runDbSql sqlFile app = do
--   x0 <- runFreeT app
--   case x0 of
--     Pure r -> return r

--     Free (GetPersons next) -> do
--       logMsg "Interpreters DbSql runDbSql GetPersons"
--       r <- liftIO $ withConnection sqlFile $ \conn -> 
--         query_ conn 
--           "SELECT personId, personName, personCountry \
--           \FROM persons"
--       runDbSql sqlFile (next r)

--     Free (GetPersonFromId i next) -> do
--       logMsg $ "Interpreters DbSql runDbSql GetPersonFromId " <> show i
--       r <- liftIO $ withConnection sqlFile $ \conn -> 
--         query conn 
--           "SELECT personId, personName, personCountry \
--           \FROM persons \
--           \WHERE personId = (?)" (Only i)
--       runDbSql sqlFile (next r)


interpretDbSql :: (MonadLogger m, MonadIO m) => FilePath -> DbF a -> m a
interpretDbSql sqlFile = \case

  GetPersons next -> do
    logMsg "Interpreters DbSql interpretDbSql GetPersons"
    fmap next $ liftIO $ withConnection sqlFile $ \conn -> 
      query_ conn 
        "SELECT personId, personName, personCountry \
        \FROM persons"

  GetPersonFromId i next -> do
    logMsg $ "Interpreters DbSql interpretDbSql GetPersonFromId " <> show i
    fmap next $ liftIO $ withConnection sqlFile $ \conn -> 
      query conn 
        "SELECT personId, personName, personCountry \
        \FROM persons \
        \WHERE personId = (?)" (Only i)

runDbSql :: (MonadLogger m, MonadIO m) => FilePath -> DbT m a -> m a
runDbSql sqlFile = iterT (join . interpretDbSql sqlFile)

-- runDbSql :: 
--   (MonadTrans t, Monad (t m), MonadLogger m, MonadLogger (t m), MonadIO (t m)) => 
--   FilePath -> DbT m b -> t m b
-- runDbSql sqlFile = myFoldFreeT (interpretDbSql sqlFile)

