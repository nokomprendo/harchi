
{-# LANGUAGE FlexibleInstances #-}
-- {-# LANGUAGE GADTs #-}

module Harchi.FreeT.Effects.Logger where

import Control.Monad.Trans.Free (FreeT(..), liftF)
-- import Control.Monad.Trans.Class -- (lift, MonadTrans)

data LoggerF a
  = LogMsg String (() -> a)
  deriving Functor

-- data LoggerF a where
--   LogMsg :: String -> (() -> a) -> LoggerF a
--   deriving Functor

type LoggerT = FreeT LoggerF

class Monad m => MonadLogger m where
  logMsg :: String -> m ()

instance Monad m => MonadLogger (LoggerT m) where
  logMsg str = liftF (LogMsg str id)


-- myFoldFreeT :: 
--   (MonadTrans t, Monad m, Monad (t m)) =>
--   (f (FreeT f m b) -> t m (FreeT f m b)) -> FreeT f m b -> t m b
-- myFoldFreeT f (FreeT m) = lift m >>= foldFreeF
--   where
--     foldFreeF (Pure a) = return a
--     foldFreeF (Free as) = f as >>= myFoldFreeT f
