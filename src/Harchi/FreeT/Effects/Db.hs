
{-# LANGUAGE FlexibleInstances #-}

module Harchi.FreeT.Effects.Db where

import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Free (FreeT, liftF)

import Harchi.FreeT.Domain.Person (Person(..))
import Harchi.FreeT.Effects.Logger (MonadLogger, logMsg)

data DbF a
  = GetPersons ([Person] -> a)
  | GetPersonFromId Int ([Person] -> a)
  deriving (Functor)

type DbT = FreeT DbF

class Monad m => MonadDb m where
  getPersons :: m [Person]
  getPersonFromId :: Int -> m [Person]

instance Monad m => MonadDb (DbT m) where
  getPersons = liftF $ GetPersons id
  getPersonFromId i = liftF $ GetPersonFromId i id

instance MonadLogger m => MonadLogger (DbT m) where
  logMsg = lift . logMsg 

