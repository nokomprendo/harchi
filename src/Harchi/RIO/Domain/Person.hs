
{-# LANGUAGE NoImplicitPrelude #-}

module Harchi.RIO.Domain.Person where

import Data.Aeson (ToJSON, toEncoding, genericToEncoding, defaultOptions)
import RIO

data Person = Person
  { personId :: Int
  , personName :: String
  , personCountry :: String
  } deriving (Eq, Generic, Show)

instance ToJSON Person where
  toEncoding = genericToEncoding defaultOptions

