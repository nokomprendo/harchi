
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

{-# OPTIONS_GHC -Wno-orphans #-} 

module Harchi.RIO.Interpreters.Env where

import Data.Text.IO (putStrLn)
import Database.SQLite.Simple (FromRow, field, fromRow, withConnection, query, query_, Only(..))
import RIO

import Harchi.RIO.Domain.Person (Person(..)) 
import Harchi.RIO.Effects.Logger (HasLogger(..)) 
import Harchi.RIO.Effects.Db (HasPersons(..)) 

data Env = Env
  { envPrefix :: !Text
  , envSqlFile :: !FilePath
  }

instance HasLogger Env where
  getLogger env str = putStrLn $ "[" <> envPrefix env <> "] " <> str

instance FromRow Person where
    fromRow = Person <$> field <*> field <*> field

instance HasPersons Env where
  persons env = 
    withConnection (envSqlFile env) $ \conn -> 
      query_ conn 
        "SELECT personId, personName, personCountry \
        \FROM persons"

  personFromId env i =
    withConnection (envSqlFile env) $ \conn -> 
      query conn 
        "SELECT personId, personName, personCountry \
        \FROM persons \
        \WHERE personId = (?)" (Only i)

