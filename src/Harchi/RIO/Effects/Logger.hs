
{-# LANGUAGE NoImplicitPrelude #-}

module Harchi.RIO.Effects.Logger where

import RIO

class HasLogger env where
  getLogger :: env -> Text -> IO ()

logMsg :: (HasLogger env) => Text -> RIO env ()
logMsg txt = do
    env <- ask
    liftIO $ getLogger env txt

