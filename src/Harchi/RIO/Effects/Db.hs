
{-# LANGUAGE NoImplicitPrelude #-}

module Harchi.RIO.Effects.Db where

import RIO

import Harchi.RIO.Domain.Person (Person)

class HasPersons env where
  persons :: env -> IO [Person]
  personFromId :: env -> Int -> IO [Person]

getPersons :: (HasPersons env) => RIO env [Person]
getPersons = do
    env <- ask
    liftIO $ persons env

getPersonFromId :: (HasPersons env) => Int -> RIO env [Person]
getPersonFromId i = do
    env <- ask
    liftIO $ personFromId env i

