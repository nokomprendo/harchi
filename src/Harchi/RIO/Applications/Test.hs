
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Harchi.RIO.Applications.Test where

import RIO

import Harchi.RIO.Domain.Person (Person)
import Harchi.RIO.Effects.Logger (logMsg) 
import Harchi.RIO.Effects.Db (getPersons, getPersonFromId) 
import Harchi.RIO.Interpreters.Env (Env(..))

testApp :: RIO Env [Person]
testApp = do
  logMsg "Test testApp getPerson 2"
  res1 <- getPersonFromId 2 
  logMsg "Test testApp getPersons"
  res2 <- getPersons
  return (res1 ++ res2)

runApp :: RIO Env [Person] -> IO [Person]
runApp app = do
  let env = Env
        { envPrefix = "RIO"
        , envSqlFile = "persons.db"
        }
  runRIO env app

