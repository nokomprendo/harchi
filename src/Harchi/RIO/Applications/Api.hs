
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Harchi.RIO.Applications.Api where

import Data.Text (pack)
import Servant ((:>), (:-), Get, Capture, JSON)
import Servant.Server.Generic (AsServerT)

import RIO

import Harchi.RIO.Domain.Person (Person)
import Harchi.RIO.Effects.Logger (HasLogger, logMsg) 
import Harchi.RIO.Effects.Db (HasPersons, getPersons, getPersonFromId) 

data Routes route = Routes
  { _home :: route :- Get '[JSON] Text
  , _persons :: route :- "persons" :> Get '[JSON] [Person]
  , _person :: route :- "person" :> Capture "i" Int :> Get '[JSON] [Person]
  } deriving (Generic)

serverRecord :: (HasLogger env, HasPersons env) => Routes (AsServerT (RIO env))
serverRecord = Routes
  { _home = do
      logMsg "Applications Api serverRecord /"
      pure "hello"
  , _persons = do
      logMsg "Applications Api serverRecord /persons"
      getPersons
  , _person = \i -> do
      logMsg $ "Applications Api serverRecord /person/" <> pack (show i) 
      getPersonFromId i
  }

