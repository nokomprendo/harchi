
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Harchi.RIO.Applications.Server where

import Network.Wai.Handler.Warp (run)
import Servant (Handler(..))
import Servant.Server.Generic (genericServeT)

import RIO hiding (Handler)
import Prelude (putStrLn)

import Harchi.RIO.Interpreters.Env (Env(..))
import Harchi.RIO.Applications.Api (serverRecord)

runServer :: Int -> Text -> FilePath -> IO ()
runServer port prefix sqlFile = do

  putStrLn $ "listening on port " ++ show port ++ "..." 

  let env = Env
        { envPrefix = prefix
        , envSqlFile = sqlFile
        }

      runApp :: RIO Env a -> Handler a 
      runApp = runRIO env

  run port $ genericServeT runApp serverRecord

