
{-# LANGUAGE OverloadedStrings #-}

{-# OPTIONS_GHC -Wno-orphans #-} 

module Harchi.PRF.Interpreters.DbSql where

import Control.Monad.Reader (liftIO)
import Database.SQLite.Simple (FromRow, field, fromRow, withConnection, query, query_, Only(..))

import Harchi.PRF.Domain.Person (Person(..))
import Harchi.PRF.Effects.Db

instance FromRow Person where
    fromRow = Person <$> field <*> field <*> field 

mkDbSql :: FilePath -> IO (Db IO)
mkDbSql sqlFile = pure $ Db getPersonsSql getPersonFromIdSql

  where

    getPersonsSql = do
      liftIO $ withConnection sqlFile $ \conn -> 
        query_ conn 
          "SELECT personId, personName, personCountry \
          \FROM persons"

    getPersonFromIdSql i = do
      liftIO $ withConnection sqlFile $ \conn -> 
        query conn 
          "SELECT personId, personName, personCountry \
          \FROM persons \
          \WHERE personId = (?)" (Only i)

