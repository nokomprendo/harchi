
module Harchi.PRF.Interpreters.Env where

import RIO

import Harchi.PRF.Domain.Person
import Harchi.PRF.Effects.Logger
import Harchi.PRF.Effects.Db
import Harchi.PRF.Interpreters.LoggerStdout
import Harchi.PRF.Interpreters.DbSql


data Env = Env 
  { envLogger :: Logger IO
  , envDb :: Db IO
  }

mkEnv :: Text -> FilePath -> IO Env
mkEnv prefix sqlFile = do
  logger <- mkLoggerStdout prefix
  db <- mkDbSql sqlFile
  pure $ Env logger db


class HasLogger env where
  getLogger :: env -> Text -> IO ()

instance HasLogger Env where
  getLogger = _getLogger . envLogger

logMsg :: (HasLogger env) => Text -> RIO env ()
logMsg txt = do
    env <- ask
    liftIO $ getLogger env txt

class HasDb env where
  getDbPersons :: env -> IO [Person]
  getDbPersonFromId :: env -> (Int -> IO [Person])


instance HasDb Env where
  getDbPersons = _getPersons . envDb
  getDbPersonFromId = _getPersonFromId . envDb

getPersons :: (MonadIO m, MonadReader env m, HasDb env) => m [Person]
getPersons = do
  env <- ask 
  liftIO $ getDbPersons env

getPersonFromId :: (MonadIO m, MonadReader env m, HasDb env) => Int -> m [Person]
getPersonFromId i = do
  env <- ask 
  liftIO $ getDbPersonFromId env i


