
{-# LANGUAGE OverloadedStrings #-}

module Harchi.PRF.Interpreters.LoggerStdout where

import Data.Text as T
import Data.Text.IO as T

import Harchi.PRF.Effects.Logger

mkLoggerStdout :: Text -> IO (Logger IO)
mkLoggerStdout prefix = 
    let f str = T.putStrLn $ "[" <> prefix <> "] " <> str
    in pure $ Logger f

-- mkLoggerStdout :: HasLogger env => RIO env (Logger IO)

