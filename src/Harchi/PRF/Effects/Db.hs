
module Harchi.PRF.Effects.Db where

import Harchi.PRF.Domain.Person (Person)

data Db m = Db
  { _getPersons :: m [Person]
  , _getPersonFromId :: Int -> m [Person]
  }
