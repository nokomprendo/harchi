
module Harchi.PRF.Effects.Logger where

import Data.Text

data Logger m = Logger
  { _getLogger :: Text -> m ()
  }

