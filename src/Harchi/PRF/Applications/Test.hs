
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Harchi.PRF.Applications.Test where

import RIO

import Harchi.PRF.Domain.Person (Person)
import Harchi.PRF.Interpreters.Env 

testApp :: RIO Env [Person]
testApp = do
  logMsg "Test testApp getPerson 2"
  res1 <- getPersonFromId 2 
  logMsg "Test testApp getPersons"
  res2 <- getPersons
  return (res1 ++ res2)

runApp :: RIO Env [Person] -> IO [Person]
runApp app = do
  env <- mkEnv "PRF" "persons.db"
  runRIO env app


