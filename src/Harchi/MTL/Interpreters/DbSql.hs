
{-# LANGUAGE OverloadedStrings #-}

{-# OPTIONS_GHC -Wno-orphans #-} 

module Harchi.MTL.Interpreters.DbSql where

import Control.Monad.IO.Class (MonadIO)
import Control.Monad.Reader (ReaderT(..), MonadReader, MonadTrans, liftIO)
import Control.Monad.Trans.Class (lift)
import Database.SQLite.Simple (withConnection, FromRow, field, Only(..), fromRow, query, query_)

import Harchi.MTL.Effects.Db (MonadDb, getPersons, getPersonFromId)
import Harchi.MTL.Effects.Logger (MonadLogger, logMsg)

import Harchi.MTL.Domain.Person (Person(..))

newtype DbSqlT m a = DbSqlT
  { unDbSqlT :: ReaderT FilePath m a
  } deriving (Functor, Applicative, Monad, MonadReader FilePath, MonadTrans)

runDbSqlT :: FilePath -> DbSqlT m a -> m a
runDbSqlT sqlFile app = runReaderT (unDbSqlT app) sqlFile

instance FromRow Person where
    fromRow = Person <$> field <*> field <*> field 

instance (MonadIO m, MonadLogger m) => MonadDb (DbSqlT m) where

  getPersons = 
    DbSqlT $ ReaderT $ \sqlFile -> do
      logMsg "Interpreters DbSql getPersons"
      liftIO $ withConnection sqlFile $ \conn -> 
        query_ conn 
          "SELECT personId, personName, personCountry \
          \FROM persons"

  getPersonFromId i = 
    DbSqlT $ ReaderT $ \sqlFile -> do
      logMsg $ "Interpreters DbSql getPersonFromId " <> show i
      liftIO $ withConnection sqlFile $ \conn -> 
        query conn 
          "SELECT personId, personName, personCountry \
          \FROM persons \
          \WHERE personId = (?)" (Only i)

instance MonadLogger m => MonadLogger (DbSqlT m) where
  logMsg = lift . logMsg

instance MonadIO m => MonadIO (DbSqlT m) where
  liftIO = lift . liftIO

