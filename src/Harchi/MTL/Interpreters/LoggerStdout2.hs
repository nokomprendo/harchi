
module Harchi.MTL.Interpreters.LoggerStdout2 where

import Control.Monad.Reader (ask, ReaderT(..), MonadReader, MonadTrans, liftIO, MonadIO, lift)

import Harchi.MTL.Effects.Logger

newtype LoggerStdoutT m a = LoggerStdoutT 
  { unLoggerStdoutT :: ReaderT String m a
  } deriving (Functor, Applicative, Monad, MonadReader String, MonadTrans)

runLoggerStdoutT :: String -> LoggerStdoutT m a -> m a
runLoggerStdoutT prefix app = runReaderT (unLoggerStdoutT app) prefix

instance (MonadIO m) => MonadLogger (LoggerStdoutT m) where

  logMsg str = do
    prefix <- ask
    liftIO $ putStrLn $ "[" <> prefix <> "] " <> str

instance MonadIO m => MonadIO (LoggerStdoutT m) where
  liftIO = lift . liftIO

