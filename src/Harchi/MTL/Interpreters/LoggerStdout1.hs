{-# LANGUAGE InstanceSigs #-}

module Harchi.MTL.Interpreters.LoggerStdout1 where

import Control.Monad.IO.Class (liftIO, MonadIO)
import Control.Monad.Trans.Class (lift, MonadTrans)

import Harchi.MTL.Effects.Logger

newtype LoggerStdoutT m a = LoggerStdoutT
  { unLoggerStdoutT :: (String -> IO ()) -> m a 
  }

runLoggerStdoutT :: String -> LoggerStdoutT m a -> m a
runLoggerStdoutT prefix app = unLoggerStdoutT app logFunc 
  where
    logFunc str = putStrLn $ "[" <> prefix <> "] " <> str

instance MonadIO m => MonadLogger (LoggerStdoutT m) where
  logMsg :: String -> LoggerStdoutT m ()
  logMsg str = LoggerStdoutT (\f -> liftIO $ f str)

instance Functor m => Functor (LoggerStdoutT m) where
  fmap f l = 
    LoggerStdoutT (fmap f . unLoggerStdoutT l)

instance Applicative m => Applicative (LoggerStdoutT m) where
  pure = 
    LoggerStdoutT . const . pure

  loggerF <*> loggerA = 
    LoggerStdoutT $ \loggerFn ->
      unLoggerStdoutT loggerF loggerFn <*> unLoggerStdoutT loggerA loggerFn

instance Monad m => Monad (LoggerStdoutT m) where
  LoggerStdoutT ma >>= f = 
    LoggerStdoutT $ \r -> do
      a <- ma r
      unLoggerStdoutT (f a) r

instance MonadTrans LoggerStdoutT where
  lift = LoggerStdoutT . const

instance MonadIO m => MonadIO (LoggerStdoutT m) where
  liftIO = lift . liftIO

