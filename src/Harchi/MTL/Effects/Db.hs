
module Harchi.MTL.Effects.Db where

import Harchi.MTL.Domain.Person (Person)

class Monad m => MonadDb m where
  getPersons :: m [Person]
  getPersonFromId :: Int -> m [Person]

