
module Harchi.MTL.Effects.Logger where

class Monad m => MonadLogger m where
  logMsg :: String -> m ()

