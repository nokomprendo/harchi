
module Harchi.MTL.Applications.Test where

import Control.Monad.IO.Class (MonadIO)

import Harchi.MTL.Domain.Person (Person)
import Harchi.MTL.Effects.Db (MonadDb, getPersons, getPersonFromId)
import Harchi.MTL.Effects.Logger (MonadLogger, logMsg)
import Harchi.MTL.Interpreters.DbSql (runDbSqlT, DbSqlT)
import Harchi.MTL.Interpreters.LoggerStdout1 (runLoggerStdoutT, LoggerStdoutT)

testApp :: (MonadLogger m, MonadDb m, MonadIO m) => m [Person]
testApp = do
  logMsg "Applications Test testApp getPerson 2"
  res1 <- getPersonFromId 2 
  logMsg "Applications Test testApp getPersons"
  res2 <- getPersons 
  return (res1 ++ res2)

runApp :: DbSqlT (LoggerStdoutT IO) a -> IO a
runApp app = runLoggerStdoutT "MTL" $ runDbSqlT "persons.db" app

