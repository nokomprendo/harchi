
module Harchi.MTL.Applications.Server where

import Network.Wai.Handler.Warp (run)
import Servant (Handler(..))
import Servant.Server.Generic (genericServeT)

import Harchi.MTL.Interpreters.DbSql (DbSqlT, runDbSqlT)
import Harchi.MTL.Interpreters.LoggerStdout1 (LoggerStdoutT, runLoggerStdoutT)
-- import Harchi.MTL.Interpreters.LoggerStdout2 (LoggerStdoutT, runLoggerStdoutT)
import Harchi.MTL.Applications.Api (serverRecord)

runServer :: Int -> String -> FilePath -> IO ()
runServer port prefix sqlFile = do

  putStrLn $ "listening on port " ++ show port ++ "..." 

  let runApp :: DbSqlT (LoggerStdoutT Handler) a -> Handler a 
      runApp app = runLoggerStdoutT prefix $ runDbSqlT sqlFile app

  run port $ genericServeT runApp serverRecord

