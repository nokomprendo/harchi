
module Harchi.ReaderT.Effects.Db where

import Control.Monad.Reader (MonadReader, MonadIO, liftIO, ask)

import Harchi.ReaderT.Domain.Person (Person)

class HasDb env where
  getDbPersons :: env -> IO [Person]
  getDbPersonFromId :: env -> (Int -> IO [Person])

getPersons :: (MonadIO m, MonadReader env m, HasDb env) => m [Person]
getPersons = do
  env <- ask 
  liftIO $ getDbPersons env

getPersonFromId :: (MonadIO m, MonadReader env m, HasDb env) => Int -> m [Person]
getPersonFromId i = do
  env <- ask 
  liftIO $ getDbPersonFromId env i

