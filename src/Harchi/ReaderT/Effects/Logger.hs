
module Harchi.ReaderT.Effects.Logger where

import Control.Monad.Reader (MonadReader, MonadIO, liftIO, ask)

class HasLogger env where
  getLogger :: env -> (String -> IO ())

logMsg :: (MonadReader env m, HasLogger env, MonadIO m) => String -> m ()
logMsg str = do
  env <- ask
  liftIO $ getLogger env str

