
module Harchi.ReaderT.Domain.Person where

import Data.Aeson (ToJSON, toEncoding, genericToEncoding, defaultOptions)
import GHC.Generics (Generic)

data Person = Person
  { personId :: Int
  , personName :: String
  , personCountry :: String
  } deriving (Eq, Generic, Show)

instance ToJSON Person where
  toEncoding = genericToEncoding defaultOptions

