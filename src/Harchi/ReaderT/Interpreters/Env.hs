
{-# LANGUAGE OverloadedStrings #-}

{-# OPTIONS_GHC -Wno-orphans #-} 

module Harchi.ReaderT.Interpreters.Env where

import Control.Monad.Reader (ReaderT)
import Database.SQLite.Simple (FromRow, field, fromRow, withConnection, query, query_, Only(..))

import Harchi.ReaderT.Domain.Person (Person(..))
import Harchi.ReaderT.Effects.Db (HasDb, getDbPersons, getDbPersonFromId)
import Harchi.ReaderT.Effects.Logger (HasLogger, getLogger)

data Env = Env 
  { _logger :: String -> IO ()
  , _sqlFile :: FilePath
  }

type AppM = ReaderT Env IO

instance HasLogger Env where
  getLogger = _logger

instance FromRow Person where
    fromRow = Person <$> field <*> field <*> field 

instance HasDb Env where
  getDbPersons env = do
    getLogger env "Interpreters Env getPersons"
    withConnection (_sqlFile env) $ \conn -> 
      query_ conn 
        "SELECT personId, personName, personCountry \
        \FROM persons"

  getDbPersonFromId env i = do
    getLogger env $ "Interpreters Env getPersonFromId " <> show i
    withConnection (_sqlFile env) $ \conn -> 
      query conn 
        "SELECT personId, personName, personCountry \
        \FROM persons \
        \WHERE personId = (?)" (Only i)

