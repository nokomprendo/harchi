
module Harchi.ReaderT.Applications.Test where

import Control.Monad.Reader (MonadReader, MonadIO, runReaderT)

import Harchi.ReaderT.Domain.Person (Person)
import Harchi.ReaderT.Effects.Db (HasDb, getPersons, getPersonFromId)
import Harchi.ReaderT.Effects.Logger (HasLogger, logMsg)
import Harchi.ReaderT.Interpreters.Env (Env(..), AppM)

testApp :: (MonadIO m, MonadReader env m, HasLogger env, HasDb env) => m [Person]
testApp = do
  logMsg "Applications Test testApp getPerson 2"
  res1 <- getPersonFromId 2 
  logMsg "Applications Test testApp getPersons"
  res2 <- getPersons 
  return (res1 ++ res2)

runApp :: AppM a -> IO a
runApp app = runReaderT app env
  where
    env = Env
      { _logger = \str -> putStrLn $ "[ReaderT] " <> str
      , _sqlFile = "persons.db"
      }

