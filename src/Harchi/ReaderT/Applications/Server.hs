
module Harchi.ReaderT.Applications.Server where

import Control.Monad.Reader (ReaderT, runReaderT)
import Network.Wai.Handler.Warp (run)
import Servant (Handler(..))
import Servant.Server.Generic (genericServeT)

import Harchi.ReaderT.Interpreters.Env (Env(..))
import Harchi.ReaderT.Applications.Api (serverRecord)

type AppM = ReaderT Env Handler

runServer :: Int -> String -> FilePath -> IO ()
runServer port prefix sqlFile = do

  putStrLn $ "listening on port " ++ show port ++ "..." 

  let env = Env
        { _logger = \str -> putStrLn $ "[" <> prefix <> "] " <> str
        , _sqlFile = sqlFile
        }

      runApp :: AppM a -> Handler a 
      runApp app = runReaderT app env

  run port $ genericServeT runApp serverRecord

