
{-# LANGUAGE FlexibleContexts #-}

module Harchi.TLHC1.Applications.Test where

import Control.Monad.IO.Class (MonadIO)

import Harchi.TLHC1.Domain.Person (Person)
import Harchi.TLHC1.Effects.MonadPerson (getPersons, getPersonFromId)
import Harchi.TLHC1.Effects.MonadLogger (logMsg)
import Harchi.TLHC1.Interpreters.AppT (AppT, runAppT, Env(..))

testApp :: MonadIO m => AppT m [Person]
testApp = do
  logMsg "Applications Test testApp getPerson 2"
  res1 <- getPersonFromId 2 
  logMsg "Applications Test testApp getPersons"
  res2 <- getPersons 
  return (res1 ++ res2)

runApp :: AppT IO a -> IO a
runApp app = runAppT app env
  where
    env = Env
      { _logger = \str -> putStrLn $ "[TLHC] " <> str
      , _sqlFile = "persons.db"
      }

