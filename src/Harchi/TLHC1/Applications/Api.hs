
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Harchi.TLHC1.Applications.Api where

import Control.Monad.Reader (MonadIO)
import Data.Text.Lazy (Text)
import GHC.Generics (Generic)
import Servant ((:>), (:-), Get, Capture, JSON)
import Servant.Server.Generic (AsServerT)

import Harchi.TLHC1.Domain.Person (Person(..))
import Harchi.TLHC1.Effects.MonadPerson (getPersons, getPersonFromId)
import Harchi.TLHC1.Effects.MonadLogger (logMsg)
import Harchi.TLHC1.Interpreters.AppT (AppT)

data Routes route = Routes
  { _home :: route :- Get '[JSON] Text
  , _persons :: route :- "persons" :> Get '[JSON] [Person]
  , _person :: route :- "person" :> Capture "i" Int :> Get '[JSON] [Person]
  } deriving (Generic)

serverRecord :: (MonadIO m) => Routes (AsServerT (AppT m))
serverRecord = Routes
  { _home = do
      logMsg "Applications Api serverRecord /"
      pure "hello"
  , _persons = do
      logMsg "Applications Api serverRecord /persons"
      getPersons
  , _person = \i -> do
      logMsg $ "Applications Api serverRecord /person/" <> show i 
      getPersonFromId i
  }

