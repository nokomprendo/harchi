
module Harchi.TLHC1.Applications.Server where

import Network.Wai.Handler.Warp (run)
import Servant (Handler(..))
import Servant.Server.Generic (genericServeT)

import Harchi.TLHC1.Interpreters.AppT (Env(..), AppT, runAppT)
import Harchi.TLHC1.Applications.Api (serverRecord)

runServer :: Int -> String -> FilePath -> IO ()
runServer port prefix sqlFile = do

  putStrLn $ "listening on port " ++ show port ++ "..." 

  let env = Env
        { _logger = \str -> putStrLn $ "[" <> prefix <> "] " <> str
        , _sqlFile = sqlFile
        }

      runApp :: AppT Handler a -> Handler a 
      runApp app = runAppT app env

  run port $ genericServeT runApp serverRecord

