
module Harchi.TLHC1.Effects.MonadPerson where

import Harchi.TLHC1.Domain.Person (Person)

class Monad m => MonadPerson m where
  getPersons :: m [Person]
  getPersonFromId :: Int -> m [Person]

