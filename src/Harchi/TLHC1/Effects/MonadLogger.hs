
module Harchi.TLHC1.Effects.MonadLogger where

class Monad m => MonadLogger m where
  logMsg :: String -> m ()

