
{-# LANGUAGE OverloadedStrings #-}

{-# OPTIONS_GHC -Wno-orphans #-} 

module Harchi.TLHC1.Interpreters.AppT where

import Control.Monad.Reader (asks, MonadReader, ReaderT, runReaderT, MonadIO, liftIO, MonadTrans)
import Database.SQLite.Simple (FromRow, field, fromRow, withConnection, query, query_, Only(..))

import Harchi.TLHC1.Domain.Person (Person(..))
import Harchi.TLHC1.Effects.MonadPerson (MonadPerson, getPersons, getPersonFromId)
import Harchi.TLHC1.Effects.MonadLogger (MonadLogger, logMsg)

data Env = Env 
  { _logger :: String -> IO ()
  , _sqlFile :: FilePath
  }

newtype AppT m a = AppT 
  { unAppT :: ReaderT Env m a 
  } deriving (Functor, Applicative, Monad, MonadReader Env, MonadIO, MonadTrans)

runAppT :: AppT m a -> Env -> m a
runAppT = runReaderT . unAppT 

instance MonadIO m => MonadLogger (AppT m) where
  logMsg str = do
    logger <- asks _logger
    liftIO $ logger str

instance FromRow Person where
    fromRow = Person <$> field <*> field <*> field 

instance (MonadIO m) => MonadPerson (AppT m) where
  getPersons = do
    sqlFile <- asks _sqlFile
    logMsg "Interpreters AppT getPersons"
    liftIO $ withConnection sqlFile $ \conn -> 
      query_ conn 
        "SELECT personId, personName, personCountry \
        \FROM persons"

  getPersonFromId i = do
    sqlFile <- asks _sqlFile
    logMsg $ "Interpreters AppT getPersonFromId " <> show i
    liftIO $ withConnection sqlFile $ \conn -> 
      query conn 
        "SELECT personId, personName, personCountry \
        \FROM persons \
        \WHERE personId = (?)" (Only i)

