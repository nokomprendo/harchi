
{-# LANGUAGE OverloadedStrings #-}

{-# OPTIONS_GHC -Wno-orphans #-} 

module Harchi.ReaderT1.Interpreters.DbSql where

import Control.Monad.Reader (liftIO, MonadIO)
import Database.SQLite.Simple (FromRow, field, fromRow, withConnection, query, query_, Only(..))

import Harchi.ReaderT1.Domain.Person (Person(..))

instance FromRow Person where
    fromRow = Person <$> field <*> field <*> field 

getPersonsSql :: (MonadIO m) => FilePath -> m [Person]
getPersonsSql sqlFile = do
  liftIO $ withConnection sqlFile $ \conn -> 
    query_ conn 
      "SELECT personId, personName, personCountry \
      \FROM persons"

getPersonFromIdSql :: (MonadIO m) => FilePath -> Int -> m [Person]
getPersonFromIdSql sqlFile i = do
  liftIO $ withConnection sqlFile $ \conn -> 
    query conn 
      "SELECT personId, personName, personCountry \
      \FROM persons \
      \WHERE personId = (?)" (Only i)

