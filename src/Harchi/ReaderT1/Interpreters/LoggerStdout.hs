
module Harchi.ReaderT1.Interpreters.LoggerStdout where

import Control.Monad.IO.Class (MonadIO, liftIO)

logMsgStdout :: MonadIO m => String -> String -> m ()
logMsgStdout prefix str = liftIO $ putStrLn $ "[" <> prefix <> "] " <> str

