
module Harchi.ReaderT1.Effects.Logger where

import Control.Monad.Reader (ask, MonadReader, MonadIO, liftIO)

class HasLogger env where
  getLogger :: env -> (String -> IO ())

logMsg :: (MonadIO m, MonadReader env m, HasLogger env) => String -> m ()
logMsg str = do
  env <- ask
  liftIO $ getLogger env str

