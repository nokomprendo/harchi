
module Harchi.ReaderT1.Effects.Db where

import Control.Monad.Reader (ask, MonadReader, MonadIO, liftIO)

import Harchi.ReaderT1.Domain.Person (Person)

class HasDb env where
  getDbPersons :: env -> IO [Person]
  getDbPersonFromId :: env -> (Int -> IO [Person])

getPersons :: (MonadIO m, MonadReader env m, HasDb env) => m [Person]
getPersons = do
  env <- ask 
  liftIO $ getDbPersons env

getPersonFromId :: (MonadIO m, MonadReader env m, HasDb env) => Int -> m [Person]
getPersonFromId i = do
  env <- ask 
  liftIO $ getDbPersonFromId env i

