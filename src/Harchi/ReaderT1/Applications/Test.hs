
module Harchi.ReaderT1.Applications.Test where

import Control.Monad.Reader (MonadReader, MonadIO, runReaderT, ReaderT)

import Harchi.ReaderT1.Domain.Person (Person)
import Harchi.ReaderT1.Effects.Db (HasDb(..), getPersons, getPersonFromId)
import Harchi.ReaderT1.Effects.Logger (HasLogger(..), logMsg)
import Harchi.ReaderT1.Interpreters.DbSql
import Harchi.ReaderT1.Interpreters.LoggerStdout

testApp :: (MonadIO m, MonadReader env m, HasLogger env, HasDb env) => m [Person]
testApp = do
  logMsg "Applications Test testApp getPerson 2"
  res1 <- getPersonFromId 2 
  logMsg "Applications Test testApp getPersons"
  res2 <- getPersons 
  return (res1 ++ res2)

runApp :: AppM a -> IO a
runApp app = runReaderT app env
  where
    env = Env
      { _logger = logMsgStdout "ReaderT1"
      , _sqlFile = "persons.db"
      }

data Env = Env 
  { _logger :: String -> IO ()
  , _sqlFile :: FilePath
  }

type AppM = ReaderT Env IO

instance HasLogger Env where
  getLogger = _logger

instance HasDb Env where
  getDbPersons env = getPersonsSql (_sqlFile env)
  getDbPersonFromId env = getPersonFromIdSql (_sqlFile env)



