
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Harchi.ReaderT1.Applications.Api where

import Control.Monad.Reader (MonadReader, MonadIO)
import Data.Text.Lazy (Text)
import GHC.Generics (Generic)
import Servant ((:>), (:-), Get, Capture, JSON)
import Servant.Server.Generic (AsServerT)

import Harchi.ReaderT1.Domain.Person (Person(..))
import Harchi.ReaderT1.Effects.Db (HasDb, getPersons, getPersonFromId)
import Harchi.ReaderT1.Effects.Logger (HasLogger, logMsg)

data Routes route = Routes
  { _home :: route :- Get '[JSON] Text
  , _persons :: route :- "persons" :> Get '[JSON] [Person]
  , _person :: route :- "person" :> Capture "i" Int :> Get '[JSON] [Person]
  } deriving (Generic)

serverRecord :: (MonadIO m, MonadReader env m, HasLogger env, HasDb env) => Routes (AsServerT m)
serverRecord = Routes
  { _home = do
      logMsg "Applications Api serverRecord /"
      pure "hello"
  , _persons = do
      logMsg "Applications Api serverRecord /persons"
      getPersons
  , _person = \i -> do
      logMsg $ "Applications Api serverRecord /person/" <> show i 
      getPersonFromId i
  }

