
module Harchi.ReaderT1.Applications.Server where

import Control.Monad.Reader (ReaderT, runReaderT)
import Network.Wai.Handler.Warp (run)
import Servant (Handler(..))
import Servant.Server.Generic (genericServeT)

import Harchi.ReaderT1.Effects.Db (HasDb, getDbPersons, getDbPersonFromId)
import Harchi.ReaderT1.Effects.Logger (HasLogger, getLogger)
import Harchi.ReaderT1.Interpreters.DbSql (getPersonsSql, getPersonFromIdSql)
import Harchi.ReaderT1.Interpreters.LoggerStdout (logMsgStdout)
import Harchi.ReaderT1.Applications.Api (serverRecord)

data Env = Env 
  { _logger :: String -> IO ()
  , _sqlFile :: FilePath
  }

type AppM = ReaderT Env Handler

instance HasLogger Env where
  getLogger = _logger

instance HasDb Env where
  getDbPersons env = getPersonsSql (_sqlFile env)
  getDbPersonFromId env = getPersonFromIdSql (_sqlFile env)

runServer :: Int -> String -> FilePath -> IO ()
runServer port prefix sqlFile = do

  putStrLn $ "listening on port " ++ show port ++ "..." 

  let env = Env
        { _logger = logMsgStdout prefix
        , _sqlFile = sqlFile
        }

      runApp :: AppM a -> Handler a 
      runApp app = runReaderT app env

  run port $ genericServeT runApp serverRecord

