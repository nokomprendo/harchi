
module Harchi.ADT.Applications.Test where

import Control.Monad.IO.Class (MonadIO)

import Harchi.ADT.Domain.Person (Person)
import Harchi.ADT.Effects.Logger (Logger, logMsg)
import Harchi.ADT.Effects.Db (Db, getPersons, getPersonFromId)
import Harchi.ADT.Interpreters.LoggerStdout (mkLoggerStdout)
import Harchi.ADT.Interpreters.DbSql (mkDbSql)

type TestApp m a = Logger m -> Db m -> m a

testApp :: MonadIO m => TestApp m [Person]
testApp logger db = do
  logMsg logger "Applications Test testApp getPerson 2"
  res1 <- getPersonFromId db 2
  logMsg logger "Applications Test testApp getPersons"
  res2 <- getPersons db 
  return (res1 ++ res2)

runApp :: TestApp IO a -> IO a
runApp app = app logger db
  where
    logger = mkLoggerStdout "ADT"
    db = mkDbSql "persons.db" logger

