
module Harchi.ADT.Applications.Server where

import Network.Wai.Handler.Warp (run)
import Servant.Server.Generic (genericServe)

import Harchi.ADT.Interpreters.DbSql (mkDbSql)
import Harchi.ADT.Interpreters.LoggerStdout (mkLoggerStdout)
import Harchi.ADT.Applications.Api (serverRecord)

runServer :: Int -> String -> FilePath -> IO ()
runServer port prefix sqlFile = do

  putStrLn $ "listening on port " ++ show port ++ "..." 

  let logger = mkLoggerStdout prefix
      db = mkDbSql sqlFile logger

  run port $ genericServe (serverRecord logger db)

