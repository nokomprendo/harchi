
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Harchi.ADT.Applications.Api where

import Control.Monad.IO.Class (liftIO)
import Data.Text (Text)
import GHC.Generics (Generic)
import Servant ((:>), (:-), Get, Capture, JSON)
import Servant.Server.Generic (AsServer)

import Harchi.ADT.Domain.Person (Person)
import Harchi.ADT.Effects.Logger (Logger(..))
import Harchi.ADT.Effects.Db (Db(..))

data Routes route = Routes
  { _home :: route :- Get '[JSON] Text
  , _persons :: route :- "persons" :> Get '[JSON] [Person]
  , _person :: route :- "person" :> Capture "i" Int :> Get '[JSON] [Person]
  } deriving (Generic)

serverRecord :: Logger IO -> Db IO -> Routes AsServer
serverRecord logger db = Routes
  { _home = liftIO $ do
      logMsg logger "Applications Api serverRecord /"
      pure "hello"
  , _persons = liftIO $ do
      logMsg logger "Applications Api serverRecord /persons"
      getPersons db
  , _person = \i -> liftIO $ do
      logMsg logger $ "Applications Api serverRecord /person/" <> show i 
      getPersonFromId db i
  }

