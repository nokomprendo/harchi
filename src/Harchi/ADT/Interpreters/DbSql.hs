
{-# LANGUAGE OverloadedStrings #-}

{-# OPTIONS_GHC -Wno-orphans #-} 

module Harchi.ADT.Interpreters.DbSql where

import Control.Monad.IO.Class (MonadIO, liftIO)
import Database.SQLite.Simple (FromRow, field, withConnection, Only(..), fromRow, query, query_)

import Harchi.ADT.Domain.Person (Person(..))
import Harchi.ADT.Effects.Db (Db(..))
import Harchi.ADT.Effects.Logger (Logger(..))

instance FromRow Person where
    fromRow = Person <$> field <*> field <*> field

mkDbSql :: MonadIO m => FilePath -> Logger m -> Db m
mkDbSql sqlFile logger = 
  Db getPersonsSql getPersonFromIdSql

  where
    getPersonsSql = do
      logMsg logger "Interpreters DbSql getPersons"
      liftIO $ withConnection sqlFile $ \conn ->
        query_ conn 
          "SELECT personId, personName, personCountry \
          \FROM persons"

    getPersonFromIdSql i = do
      logMsg logger $ "Interpreters DbSql getPersonFromId " <> show i 
      liftIO $ withConnection sqlFile $ \conn -> 
        query conn 
          "SELECT personId, personName, personCountry \
          \FROM persons \
          \WHERE personId = (?)" (Only i)

