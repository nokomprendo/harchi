
module Harchi.ADT.Interpreters.LoggerStdout where

import Control.Monad.IO.Class (MonadIO, liftIO)

import Harchi.ADT.Effects.Logger

mkLoggerStdout :: MonadIO m => String -> Logger m
mkLoggerStdout prefix = Logger logMsgStdout
  where
    logMsgStdout str = liftIO $ putStrLn $ "[" <> prefix <> "] " <> str

