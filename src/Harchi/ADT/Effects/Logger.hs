
module Harchi.ADT.Effects.Logger where

newtype Logger m = Logger
  { logMsg :: String -> m ()
  }

