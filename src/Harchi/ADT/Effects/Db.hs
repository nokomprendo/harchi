
module Harchi.ADT.Effects.Db where

import Harchi.ADT.Domain.Person (Person)

data Db m = Db
  { getPersons :: m [Person]
  , getPersonFromId :: Int -> m [Person]
  }

