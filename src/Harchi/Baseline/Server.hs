
module Harchi.Baseline.Server where

import Network.Wai.Handler.Warp (run)
import Servant.Server.Generic (genericServe)

import Harchi.Baseline.Api (serverRecord)

runServer :: Int -> String -> FilePath -> IO ()
runServer port prefix sqlFile = do
  putStrLn $ "listening on port " ++ show port ++ "..." 
  run port $ genericServe (serverRecord prefix sqlFile)

