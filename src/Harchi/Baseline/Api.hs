
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Harchi.Baseline.Api where

import Control.Monad.IO.Class (liftIO)
import Data.Text (Text)
import GHC.Generics (Generic)
import Servant ((:>), (:-), Get, Capture, JSON)
import Servant.Server.Generic (AsServer)

import Harchi.Baseline.Person (Person)
import Harchi.Baseline.LoggerStdout (logMsg)
import Harchi.Baseline.DbSql (getPersons, getPersonFromId)

data Routes route = Routes
  { _home :: route :- Get '[JSON] Text
  , _persons :: route :- "persons" :> Get '[JSON] [Person]
  , _person :: route :- "person" :> Capture "i" Int :> Get '[JSON] [Person]
  } deriving (Generic)

serverRecord :: String -> FilePath -> Routes AsServer
serverRecord prefix sqlFile = Routes
  { _home = liftIO $ do
      logMsg prefix "Api serverRecord /"
      pure "hello"
  , _persons = liftIO $ do
      logMsg prefix "Api serverRecord /persons"
      getPersons prefix sqlFile
  , _person = \i -> liftIO $ do
      logMsg prefix $ "Api serverRecord /person/" <> show i 
      getPersonFromId prefix sqlFile i
  }

