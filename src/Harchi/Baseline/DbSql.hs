
{-# LANGUAGE OverloadedStrings #-}

{-# OPTIONS_GHC -Wno-orphans #-} 

module Harchi.Baseline.DbSql where

import Database.SQLite.Simple (FromRow, field, withConnection, Only(..), fromRow, query, query_)

import Harchi.Baseline.Person (Person(..))
import Harchi.Baseline.LoggerStdout (logMsg)

instance FromRow Person where
    fromRow = Person <$> field <*> field <*> field

getPersons :: String -> FilePath -> IO [Person]
getPersons prefix sqlFile = do
  logMsg prefix "DbSql getPersons"
  withConnection sqlFile $ \conn ->
    query_ conn 
      "SELECT personId, personName, personCountry \
      \FROM persons"

getPersonFromId :: String -> FilePath -> Int -> IO [Person]
getPersonFromId prefix sqlFile i = do
  logMsg prefix $ "DbSql getPersonFromId " <> show i 
  withConnection sqlFile $ \conn -> 
    query conn 
      "SELECT personId, personName, personCountry \
      \FROM persons \
      \WHERE personId = (?)" (Only i)

