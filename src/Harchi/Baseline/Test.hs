
module Harchi.Baseline.Test where

import Harchi.Baseline.Person (Person)
import Harchi.Baseline.LoggerStdout (logMsg)
import Harchi.Baseline.DbSql (getPersons, getPersonFromId)

type TestApp a = String -> FilePath -> IO a

testApp :: TestApp [Person]
testApp prefix sqlFile = do
  logMsg prefix "Test testApp getPerson 2"
  res1 <- getPersonFromId prefix sqlFile 2
  logMsg prefix "Test testApp getPersons"
  res2 <- getPersons prefix sqlFile
  return (res1 ++ res2)

runApp :: TestApp a -> IO a
runApp app = app "Baseline" "persons.db"

