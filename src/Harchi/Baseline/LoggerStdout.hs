
module Harchi.Baseline.LoggerStdout where

logMsg :: String -> String -> IO ()
logMsg prefix str = putStrLn $ "[" <> prefix <> "] " <> str

