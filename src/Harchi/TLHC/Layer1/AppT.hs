
module Harchi.TLHC.Layer1.AppT where

import Control.Monad.Reader (MonadReader, ReaderT, runReaderT, MonadIO, MonadTrans)

data Env = Env 
  { _logger :: String -> IO ()
  , _sqlFile :: FilePath
  }

newtype AppT m a = AppT 
  { unAppT :: ReaderT Env m a 
  } deriving (Functor, Applicative, Monad, MonadReader Env, MonadIO, MonadTrans)

runAppT :: AppT m a -> Env -> m a
runAppT = runReaderT . unAppT 

