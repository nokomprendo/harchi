
{-# LANGUAGE FlexibleContexts #-}

module Harchi.TLHC.Layer1.Test where

import Control.Monad.IO.Class (MonadIO)

import Harchi.TLHC.Layer3.Person (Person)
import Harchi.TLHC.Layer2.MonadPerson (getPersons, getPersonFromId)
import Harchi.TLHC.Layer2.MonadLogger (logMsg)
import Harchi.TLHC.Layer1.AppT (AppT, runAppT, Env(..))

testApp :: MonadIO m => AppT m [Person]
testApp = do
  logMsg "Layer1 Test testApp getPerson 2"
  res1 <- getPersonFromId 2 
  logMsg "Layer1 Test testApp getPersons"
  res2 <- getPersons 
  return (res1 ++ res2)

runApp :: AppT IO a -> IO a
runApp app = runAppT app env
  where
    env = Env
      { _logger = \str -> putStrLn $ "[TLHC] " <> str
      , _sqlFile = "persons.db"
      }

