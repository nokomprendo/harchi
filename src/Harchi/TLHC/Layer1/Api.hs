
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Harchi.TLHC.Layer1.Api where

import Control.Monad.Reader (MonadIO)
import Data.Text.Lazy (Text)
import GHC.Generics (Generic)
import Servant ((:>), (:-), Get, Capture, JSON)
import Servant.Server.Generic (AsServerT)

import Harchi.TLHC.Layer3.Person (Person(..))
import Harchi.TLHC.Layer2.MonadPerson (getPersons, getPersonFromId)
import Harchi.TLHC.Layer2.MonadLogger (logMsg)
import Harchi.TLHC.Layer1.AppT (AppT)

data Routes route = Routes
  { _home :: route :- Get '[JSON] Text
  , _persons :: route :- "persons" :> Get '[JSON] [Person]
  , _person :: route :- "person" :> Capture "i" Int :> Get '[JSON] [Person]
  } deriving (Generic)

serverRecord :: (MonadIO m) => Routes (AsServerT (AppT m))
serverRecord = Routes
  { _home = do
      logMsg "Applications Api serverRecord /"
      pure "hello"
  , _persons = do
      logMsg "Applications Api serverRecord /persons"
      getPersons
  , _person = \i -> do
      logMsg $ "Applications Api serverRecord /person/" <> show i 
      getPersonFromId i
  }

