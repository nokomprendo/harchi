
module Harchi.TLHC.Layer1.Server where

import Network.Wai.Handler.Warp (run)
import Servant (Handler(..))
import Servant.Server.Generic (genericServeT)

import Harchi.TLHC.Layer1.AppT (Env(..), AppT, runAppT)
import Harchi.TLHC.Layer1.Api (serverRecord)

runServer :: Int -> String -> FilePath -> IO ()
runServer port prefix sqlFile = do

  putStrLn $ "listening on port " ++ show port ++ "..." 

  let env = Env
        { _logger = \str -> putStrLn $ "[" <> prefix <> "] " <> str
        , _sqlFile = sqlFile
        }

      runApp :: AppT Handler a -> Handler a 
      runApp app = runAppT app env

  run port $ genericServeT runApp serverRecord

