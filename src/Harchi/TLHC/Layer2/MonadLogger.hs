
module Harchi.TLHC.Layer2.MonadLogger where

import Control.Monad.Reader (asks, MonadIO, liftIO)

import Harchi.TLHC.Layer1.AppT (AppT, Env(..))

class Monad m => MonadLogger m where
  logMsg :: String -> m ()

instance MonadIO m => MonadLogger (AppT m) where
  logMsg str = do
    logger <- asks _logger
    liftIO $ logger str

