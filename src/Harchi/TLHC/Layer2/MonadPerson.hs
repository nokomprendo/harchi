
{-# LANGUAGE OverloadedStrings #-}

{-# OPTIONS_GHC -Wno-orphans #-} 

module Harchi.TLHC.Layer2.MonadPerson where

import Control.Monad.Reader (asks, MonadIO, liftIO)
import Database.SQLite.Simple (FromRow, field, fromRow, withConnection, query, query_, Only(..))

import Harchi.TLHC.Layer3.Person (Person(..))
import Harchi.TLHC.Layer2.MonadLogger (logMsg)
import Harchi.TLHC.Layer1.AppT (AppT, Env(..))

class Monad m => MonadPerson m where
  getPersons :: m [Person]
  getPersonFromId :: Int -> m [Person]

instance FromRow Person where
    fromRow = Person <$> field <*> field <*> field 

instance (MonadIO m) => MonadPerson (AppT m) where
  getPersons = do
    sqlFile <- asks _sqlFile
    logMsg "Layer2 MonadPerson getPersons"
    liftIO $ withConnection sqlFile $ \conn -> 
      query_ conn 
        "SELECT personId, personName, personCountry \
        \FROM persons"

  getPersonFromId i = do
    sqlFile <- asks _sqlFile
    logMsg $ "Layer2 MonadPerson getPersonFromId " <> show i
    liftIO $ withConnection sqlFile $ \conn -> 
      query conn 
        "SELECT personId, personName, personCountry \
        \FROM persons \
        \WHERE personId = (?)" (Only i)
