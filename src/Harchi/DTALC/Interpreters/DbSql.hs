
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

{-# OPTIONS_GHC -Wno-orphans #-} 

module Harchi.DTALC.Interpreters.DbSql where

import Control.Monad.Free (Free)
import Control.Monad.IO.Class (liftIO, MonadIO)
import Database.SQLite.Simple (FromRow, field, withConnection, Only(..), fromRow, query, query_)

import Harchi.DTALC.Domain.Person (Person(..))
import Harchi.DTALC.Effects.Db 
import Harchi.DTALC.Utils

-------------------------------------------------------------------------------

instance FromRow Person where
  fromRow = Person <$> field <*> field <*> field 

-------------------------------------------------------------------------------

runDbSql :: (MonadIO m, DbSql f) => FilePath -> Free f a -> m a
runDbSql sqlFile = myFoldFree pure (dbAlgebra sqlFile)

class Functor f => DbSql f where
  dbAlgebra :: MonadIO m => FilePath -> f (m a) -> m a

-------------------------------------------------------------------------------

instance DbSql DbF  where
  dbAlgebra sqlFile = \case

    GetPersons next -> do
      r <- liftIO $ withConnection sqlFile $ \conn -> 
        query_ conn 
          "SELECT personId, personName, personCountry \
          \FROM persons"
      next r

    GetPersonFromId i next -> do
      r <- liftIO $ withConnection sqlFile $ \conn -> 
        query conn 
          "SELECT personId, personName, personCountry \
          \FROM persons \
          \WHERE personId = (?)" (Only i)
      next r

