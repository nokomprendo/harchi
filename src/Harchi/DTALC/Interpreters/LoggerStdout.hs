
{-# LANGUAGE LambdaCase #-}

module Harchi.DTALC.Interpreters.LoggerStdout where

import Control.Monad.Free (Free(..))
import Control.Monad.IO.Class (MonadIO, liftIO)

import Harchi.DTALC.Effects.Logger 
import Harchi.DTALC.Utils

-------------------------------------------------------------------------------

runLoggerStdout :: (MonadIO m, LoggerStdout f) => String -> Free f a -> m a
runLoggerStdout prefix = myFoldFree pure (loggerAlgebra prefix)

class Functor f => LoggerStdout f where
  loggerAlgebra :: MonadIO m => String -> f (m a) -> m a

-------------------------------------------------------------------------------

instance LoggerStdout LoggerF  where
  loggerAlgebra prefix = \case
    LogMsg str next -> do
      liftIO $ putStrLn $ "["  <> prefix <> "] " <> str
      next

-------------------------------------------------------------------------------

appLogger1 :: Free LoggerF ()
appLogger1 = do
    logMsg "foo"
    logMsg "bar"

-- >>> runLoggerStdout "test" appLogger1
-- [test] foo
-- [test] bar
--

