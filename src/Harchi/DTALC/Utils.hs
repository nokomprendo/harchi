
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Harchi.DTALC.Utils where

import Control.Monad.Free

-------------------------------------------------------------------------------

data (f :+: g) e
  = InL (f e) 
  | InR (g e)

infixr 5 :+:

instance (Functor f, Functor g) => Functor (f :+: g) where
  fmap f = \case
    InL e1 -> InL (fmap f e1)
    InR e2 -> InR (fmap f e2)

-------------------------------------------------------------------------------

class (Functor sub, Functor sup) => sub :<: sup where
  inj :: sub a -> sup a
  prj :: sup a -> Maybe (sub a)

instance Functor f => f :<: f where
  inj = id
  prj = Just

instance {-# OVERLAPS #-}
  (Functor f, Functor g) => 
  f :<: (f :+: g) where

  inj = InL
  prj = \case
    (InL x) -> prj x
    _ -> Nothing

instance {-# OVERLAPS #-}
  (Functor f, Functor g, Functor h, f :<: g) => 
  f :<: (h :+: g) where

  inj = InR . inj
  prj = \case
    (InR y) -> prj y
    _ -> Nothing

-------------------------------------------------------------------------------

inject :: (g :<: f) => g (Free f a) -> Free f a
inject = Free . inj

myFoldFree :: Functor f => (a -> b) -> (f b -> b) -> Free f a -> b
myFoldFree fPure fFree = \case
  Pure x -> fPure x
  Free t -> fFree (fmap (myFoldFree fPure fFree) t)

