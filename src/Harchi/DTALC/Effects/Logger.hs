
{-# LANGUAGE FlexibleContexts #-}

module Harchi.DTALC.Effects.Logger where

import Control.Monad.Free (Free(..))

import Harchi.DTALC.Utils

data LoggerF a
  = LogMsg String a
  deriving Functor

logMsg :: (LoggerF :<: f) => String -> Free f ()
logMsg str = inject (LogMsg str (Pure ()))

