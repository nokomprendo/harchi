
{-# LANGUAGE FlexibleContexts #-}

module Harchi.DTALC.Effects.Db where

import Control.Monad.Free (Free(..))

import Harchi.DTALC.Domain.Person (Person(..))
import Harchi.DTALC.Utils

data DbF a 
  = GetPersons ([Person] -> a)
  | GetPersonFromId Int ([Person] -> a)
  deriving Functor

getPersons :: (DbF :<: f) => Free f [Person]
getPersons = inject (GetPersons Pure)

getPersonFromId :: (DbF :<: f) => Int -> Free f [Person]
getPersonFromId i = inject (GetPersonFromId i Pure)

