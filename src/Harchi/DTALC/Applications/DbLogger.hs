
{-# LANGUAGE FlexibleContexts #-}

module Harchi.DTALC.Applications.DbLogger where

import Control.Monad.IO.Class (MonadIO)
import Control.Monad.Free

import Harchi.DTALC.Effects.Db 
import Harchi.DTALC.Effects.Logger 
import Harchi.DTALC.Interpreters.DbSql 
import Harchi.DTALC.Interpreters.LoggerStdout 
import Harchi.DTALC.Utils

class Functor f => DbLogger f where
  dbLoggerAlgebra :: MonadIO m => String -> FilePath -> f (m a) -> m a

instance (LoggerStdout f, DbSql g) => DbLogger (f :+: g) where
  dbLoggerAlgebra prefix _sqlFile (InL r) = loggerAlgebra prefix r
  dbLoggerAlgebra _prefix sqlFile (InR r) = dbAlgebra sqlFile r

type App = Free (LoggerF :+: DbF)

runDbLogger :: (MonadIO m) => String -> FilePath -> App a -> m a
runDbLogger prefix sqlFile = myFoldFree pure (dbLoggerAlgebra prefix sqlFile)

