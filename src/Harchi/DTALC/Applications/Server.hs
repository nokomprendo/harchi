
module Harchi.DTALC.Applications.Server where

import Network.Wai.Handler.Warp (run)
import Servant (Handler(..))
import Servant.Server.Generic (genericServeT)

import Harchi.DTALC.Applications.Api (serverRecord)
import Harchi.DTALC.Applications.DbLogger 

runServer :: Int -> String -> FilePath -> IO ()
runServer port prefix sqlFile = do

  putStrLn $ "listening on port " ++ show port ++ "..." 

  let 
      runApp :: App a -> Handler a 
      runApp = runDbLogger prefix sqlFile

  run port $ genericServeT runApp serverRecord


