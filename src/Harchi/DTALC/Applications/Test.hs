
{-# LANGUAGE FlexibleContexts #-}

module Harchi.DTALC.Applications.Test where

import Harchi.DTALC.Domain.Person (Person)
import Harchi.DTALC.Effects.Db 
import Harchi.DTALC.Effects.Logger 
import Harchi.DTALC.Applications.DbLogger 

-- import Control.Monad.Free
-- import Harchi.DTALC.Utils

-- testApp :: (DbF :<: f, LoggerF :<: f) => Free f [Person]
testApp :: App [Person]
testApp = do
  logMsg "Applications Test testApp getPerson 2"
  res1 <- getPersonFromId 2 
  logMsg "Applications Test testApp getPersons"
  res2 <- getPersons 
  pure (res1 ++ res2)

runApp :: App a -> IO a
runApp = runDbLogger "DTALC" "persons.db" 

