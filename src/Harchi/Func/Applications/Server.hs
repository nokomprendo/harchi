
module Harchi.Func.Applications.Server where

import Network.Wai.Handler.Warp (run)
import Servant.Server.Generic (genericServe)

import Harchi.Func.Interpreters.DbSql (getPersonsSql, getPersonFromIdSql)
import Harchi.Func.Interpreters.LoggerStdout (logMsgStdout)
import Harchi.Func.Applications.Api (serverRecord)

runServer :: Int -> String -> FilePath -> IO ()
runServer port prefix sqlFile = do
  putStrLn $ "listening on port " ++ show port ++ "..." 

  let _logMsg = logMsgStdout prefix
      _getPersons = getPersonsSql sqlFile _logMsg
      _getPersonFromId = getPersonFromIdSql sqlFile _logMsg

  run port $ genericServe (serverRecord _logMsg _getPersons _getPersonFromId)

