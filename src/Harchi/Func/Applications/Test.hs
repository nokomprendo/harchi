
module Harchi.Func.Applications.Test where

import Control.Monad.IO.Class (MonadIO)

import Harchi.Func.Domain.Person (Person)
import Harchi.Func.Effects.Logger (LogMsg)
import Harchi.Func.Effects.Db (GetPersons, GetPersonFromId)
import Harchi.Func.Interpreters.LoggerStdout (logMsgStdout)
import Harchi.Func.Interpreters.DbSql (getPersonsSql, getPersonFromIdSql)

type TestApp m a = LogMsg m -> GetPersons m -> GetPersonFromId m -> m a

testApp :: MonadIO m => TestApp m [Person]
testApp logMsg getPersons getPersonFromId = do
  logMsg "Applications Test testApp getPerson 2"
  res1 <- getPersonFromId 2
  logMsg "Applications Test testApp getPersons"
  res2 <- getPersons
  return (res1 ++ res2)

runApp :: TestApp IO a -> IO a
runApp app = app _logMsg _getPersons _getPersonFromId
    where
      _logMsg = logMsgStdout "Func"
      _getPersons = getPersonsSql "persons.db" _logMsg
      _getPersonFromId = getPersonFromIdSql "persons.db" _logMsg

