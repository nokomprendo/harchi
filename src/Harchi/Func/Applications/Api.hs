
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Harchi.Func.Applications.Api where

import Control.Monad.IO.Class (liftIO)
import Data.Text (Text)
import GHC.Generics (Generic)
import Servant ((:>), (:-), Get, Capture, JSON)
import Servant.Server.Generic (AsServer)

import Harchi.Func.Domain.Person (Person)
import Harchi.Func.Effects.Logger (LogMsg)
import Harchi.Func.Effects.Db (GetPersons, GetPersonFromId)

data Routes route = Routes
  { _home :: route :- Get '[JSON] Text
  , _persons :: route :- "persons" :> Get '[JSON] [Person]
  , _person :: route :- "person" :> Capture "i" Int :> Get '[JSON] [Person]
  } deriving (Generic)

serverRecord :: LogMsg IO -> GetPersons IO -> GetPersonFromId IO -> Routes AsServer
serverRecord logMsg getPersons getPersonFromId = Routes
  { _home = liftIO $ do
      logMsg "Applications Api serverRecord /"
      pure "hello"
  , _persons = liftIO $ do
      logMsg "Applications Api serverRecord /persons"
      getPersons 
  , _person = \i -> liftIO $ do
      logMsg $ "Applications Api serverRecord /person/" <> show i 
      getPersonFromId i
  }

