
module Harchi.Func.Interpreters.LoggerStdout where

import Control.Monad.IO.Class (MonadIO, liftIO)

import Harchi.Func.Effects.Logger (LogMsg)

logMsgStdout :: MonadIO m => String -> LogMsg m
logMsgStdout prefix str = liftIO $ putStrLn $ "[" <> prefix <> "] " <> str

