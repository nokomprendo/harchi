
{-# LANGUAGE OverloadedStrings #-}

{-# OPTIONS_GHC -Wno-orphans #-} 

module Harchi.Func.Interpreters.DbSql where

import Control.Monad.IO.Class (MonadIO, liftIO)
import Database.SQLite.Simple (FromRow, field, withConnection, Only(..), fromRow, query, query_)

import Harchi.Func.Domain.Person (Person(..))
import Harchi.Func.Effects.Logger (LogMsg)
import Harchi.Func.Effects.Db (GetPersons, GetPersonFromId)

instance FromRow Person where
    fromRow = Person <$> field <*> field <*> field

getPersonsSql :: MonadIO m => FilePath -> LogMsg m -> GetPersons m
getPersonsSql sqlFile logMsg = do
  logMsg "Interpreters DbSql getPersons"
  liftIO $ withConnection sqlFile $ \conn ->
    query_ conn 
      "SELECT personId, personName, personCountry \
      \FROM persons"

getPersonFromIdSql :: MonadIO m => FilePath -> LogMsg m -> GetPersonFromId m
getPersonFromIdSql sqlFile logMsg i = do
  logMsg $ "Interpreters DbSql getPersonFromId " <> show i 
  liftIO $ withConnection sqlFile $ \conn -> 
    query conn 
      "SELECT personId, personName, personCountry \
      \FROM persons \
      \WHERE personId = (?)" (Only i)

