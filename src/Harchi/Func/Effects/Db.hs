
module Harchi.Func.Effects.Db where

import Harchi.Func.Domain.Person (Person)

type GetPersons m = m [Person]

type GetPersonFromId m = Int -> m [Person]

