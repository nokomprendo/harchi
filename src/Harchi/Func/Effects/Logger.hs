
module Harchi.Func.Effects.Logger where

type LogMsg m = String -> m ()

